# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.2.0] - 2023-11-23

### Fixed

- `MultiHeap<TVal>` implementations of `IEnumerable<TVal>.GetEnumerator` and `IEnumerable.GetEnumerator`
  yielded different results. `IEnumerable.GetEnumerator` now yields the same results of `IEnumerable<TVal>.GetEnumerator`.

## [3.1.1] - 2022-11-19

### Changed

- The project will target .NET Standard 2.0.

## [3.1.0] - 2021-11-28

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.5.2, 4.6.1 and 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

## [3.0.1] - 2020-12-06

### Fixed

- Issue #3, RemoveMin on a Fibonacci heap with a single item did not clean internal MinTree.

## [3.0.0] - 2020-08-23

### Changed

- Updated project structure.
- Renamed root namespace from DIBRIS.Hippie to PommaLabs.Hippie.
- Renamed NuGet package from DIBRIS.Hippie to PommaLabs.Hippie.
- Library is compiled for .NET Standard 2.0 and .NET Framework 4.6.1, 4.7.2.

[3.2.0]: https://gitlab.com/pommalabs/hippie/-/compare/3.1.1...3.2.0
[3.1.1]: https://gitlab.com/pommalabs/hippie/-/compare/3.1.0...3.1.1
[3.1.0]: https://gitlab.com/pommalabs/hippie/-/compare/3.0.1...3.1.0
[3.0.1]: https://gitlab.com/pommalabs/hippie/-/compare/3.0.0...3.0.1
[3.0.0]: https://gitlab.com/pommalabs/hippie/-/compare/v2.10.1...3.0.0
