﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.Hippie.Examples;

public static class Program
{
    public static void Main()
    {
        Console.WriteLine("### HeapSort");
        HeapSort.Run();
        Console.WriteLine();

        Console.WriteLine("### Weighted message");
        WeightedMessage.Run();
        Console.WriteLine();

        Console.WriteLine("### Message with key");
        RawMessage.Run();
        Console.WriteLine();

        Console.WriteLine("### Stable heap");
        StableHeap.Run();
        Console.WriteLine();

        Console.Write("Press any key to exit...");
        Console.Read();
    }
}
