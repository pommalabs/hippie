﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Attributes;
using PommaLabs.Hippie.UnitTests;

namespace PommaLabs.Hippie.Benchmarks;

public class DijkstraBenchmark : AbstractHeapBenchmark
{
    private RandomGraph _graph;
    private IHeap<int, int> _heap;

    [Params(0.07, 0.21)] // 0.07 for a sparse graph, 0.21 for a dense one.
    public double EdgeProbability { get; set; }

    [Benchmark]
    public void HeapSort()
    {
        _graph.Dijkstra(_heap, 0);
    }

    [IterationSetup]
    public void IterationCleanup()
    {
        _heap.Clear();
        _heap = null;

        _graph.Clear();
        _graph = null;
    }

    [IterationSetup]
    public void IterationSetup()
    {
        _heap = GetHeap();

        _graph = new RandomGraph(ItemCount, EdgeProbability);
    }
}
