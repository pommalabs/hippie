﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Attributes;

namespace PommaLabs.Hippie.Benchmarks;

public class RemoveMinBenchmark : AbstractHeapBenchmark
{
    private IHeap<int, int> _heap;

    [IterationSetup]
    public void IterationCleanup()
    {
        _heap.Clear();
        _heap = null;
    }

    [IterationSetup]
    public void IterationSetup()
    {
        _heap = GetHeap();
        AddRandomItemsHelper(_heap);
    }

    [Benchmark]
    public void RemoveMinUntilEmpty()
    {
        RemoveMinUntilEmptyHelper(_heap);
    }
}
