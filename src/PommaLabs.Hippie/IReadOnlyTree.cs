﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Hippie;

/// <summary>
///   A simple interface modeling a tree, whose nodes have a variable number of children.
/// </summary>
/// <typeparam name="TVal">The type of the value contained in the root of this tree.</typeparam>
/// <typeparam name="TPr">The type of the priority contained in the root of this tree.</typeparam>
public interface IReadOnlyTree<TVal, TPr>
{
    /// <summary>
    ///   The nodes which have this node as parent.
    /// </summary>
    IEnumerable<IReadOnlyTree<TVal, TPr>> Children { get; }

    /// <summary>
    ///   The node this tree is child of.
    /// </summary>
    IReadOnlyTree<TVal, TPr> Parent { get; }

    /// <summary>
    ///   The priority contained in the root of this tree.
    /// </summary>
    TPr Priority { get; }

    /// <summary>
    ///   The value contained in the root of this tree.
    /// </summary>
    TVal Value { get; }

    /// <summary>
    ///   Visits the tree in a breadth-first fashion.
    /// </summary>
    /// <returns>Each tree node in breadth-first order.</returns>
    IEnumerable<IReadOnlyTree<TVal, TPr>> BreadthFirstVisit();

    /// <summary>
    ///   Applies given function on each node in a breadth-first fashion.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    void BreadthFirstVisit(Action<IReadOnlyTree<TVal, TPr>> visitor);

    /// <summary>
    ///   Applies given function on each node in a breadth-first fashion, and returns in that
    ///   order the results produced during the visit. Result produced by a node is passed as a
    ///   parameter to all its children.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    /// <param name="start">The value passed to the root.</param>
    /// <returns>
    ///   The results, in breadth-first order, produced by <paramref name="visitor"/> while
    ///   visiting the tree.
    /// </returns>
    IEnumerable<TRet> BreadthFirstVisit<TRet>(Func<IReadOnlyTree<TVal, TPr>, TRet, TRet> visitor, TRet start);

    /// <summary>
    ///   Visits the tree in a depth-first fashion.
    /// </summary>
    /// <returns>Each tree node in depth-first order.</returns>
    IEnumerable<IReadOnlyTree<TVal, TPr>> DepthFirstVisit();

    /// <summary>
    ///   Applies given function on each node in a depth-first fashion.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    void DepthFirstVisit(Action<IReadOnlyTree<TVal, TPr>> visitor);

    /// <summary>
    ///   Applies given function on each node in a depth-first fashion, and returns in that
    ///   order the results produced during the visit. Result produced by a node is passed as a
    ///   parameter to all its children.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    /// <param name="start">The value passed to the root.</param>
    /// <returns>
    ///   The results, in depth-first order, produced by <paramref name="visitor"/> while
    ///   visiting the tree.
    /// </returns>
    IEnumerable<TRet> DepthFirstVisit<TRet>(Func<IReadOnlyTree<TVal, TPr>, TRet, TRet> visitor, TRet start);
}

/// <summary>
///   A simple interface modeling a tree, whose nodes have a variable number of children.
/// </summary>
/// <typeparam name="TItem">The type of the item contained in the root of this tree.</typeparam>
public interface IReadOnlyTree<TItem>
{
    /// <summary>
    ///   The nodes which have this node as parent.
    /// </summary>
    IEnumerable<IReadOnlyTree<TItem>> Children { get; }

    /// <summary>
    ///   The item contained in the root of this tree.
    /// </summary>
    TItem Item { get; }

    /// <summary>
    ///   The node this tree is child of.
    /// </summary>
    IReadOnlyTree<TItem> Parent { get; }

    /// <summary>
    ///   Visits the tree in a breadth-first fashion.
    /// </summary>
    /// <returns>Each tree node in breadth-first order.</returns>
    IEnumerable<IReadOnlyTree<TItem>> BreadthFirstVisit();

    /// <summary>
    ///   Applies given function on each node in a breadth-first fashion.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    void BreadthFirstVisit(Action<IReadOnlyTree<TItem>> visitor);

    /// <summary>
    ///   Applies given function on each node in a breadth-first fashion, and returns in that
    ///   order the results produced during the visit. Result produced by a node is passed as a
    ///   parameter to all its children.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    /// <param name="start">The value passed to the root.</param>
    /// <returns>
    ///   The results, in breadth-first order, produced by <paramref name="visitor"/> while
    ///   visiting the tree.
    /// </returns>
    IEnumerable<TRet> BreadthFirstVisit<TRet>(Func<IReadOnlyTree<TItem>, TRet, TRet> visitor, TRet start);

    /// <summary>
    ///   Visits the tree in a depth-first fashion.
    /// </summary>
    /// <returns>Each tree node in depth-first order.</returns>
    IEnumerable<IReadOnlyTree<TItem>> DepthFirstVisit();

    /// <summary>
    ///   Applies given function on each node in a depth-first fashion.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    void DepthFirstVisit(Action<IReadOnlyTree<TItem>> visitor);

    /// <summary>
    ///   Applies given function on each node in a depth-first fashion, and returns in that
    ///   order the results produced during the visit. Result produced by a node is passed as a
    ///   parameter to all its children.
    /// </summary>
    /// <param name="visitor">The function applied on each node.</param>
    /// <param name="start">The value passed to the root.</param>
    /// <returns>
    ///   The results, in depth-first order, produced by <paramref name="visitor"/> while
    ///   visiting the tree.
    /// </returns>
    IEnumerable<TRet> DepthFirstVisit<TRet>(Func<IReadOnlyTree<TItem>, TRet, TRet> visitor, TRet start);
}
