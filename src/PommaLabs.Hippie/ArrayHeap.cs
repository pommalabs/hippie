﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using PommaLabs.Hippie.Core;

namespace PommaLabs.Hippie;

public sealed class ArrayHeap<TVal, TPr> : RawHeap<TVal, TPr, ArrayHeap<TVal, TPr>.ArrayHandle>
{
    /// <summary>
    ///   The index from which handles are stored.
    /// </summary>
    private const int MinIndex = 0;

    /// <summary>
    ///   The minimum size of the array containing the handles.
    /// </summary>
    private const int MinSize = 4;

    /// <summary>
    ///   The factor used to increment the size of the array containing the handles.
    /// </summary>
    private const int ResizeFactor = 2;

    /// <summary>
    ///   The maximum number of children each node can have.
    /// </summary>
    private readonly byte _childCount;

    /// <summary>
    ///   The array into which handles are stored.
    /// </summary>
    private ArrayHandle[] _handles;

    internal ArrayHeap(byte childCount, IComparer<TPr> comparer)
        : base(comparer)
    {
        // Preconditions
        if (childCount < 2) throw new ArgumentOutOfRangeException(nameof(childCount), ErrorMessages.WrongChildCount);

        _childCount = childCount;
        _handles = new ArrayHandle[MinSize];
    }

    /// <inheritdoc/>
    public override IHeapHandle<TVal, TPr> Min
    {
        get
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

            var result = _handles[MinIndex];

            // Postconditions
            Debug.Assert(Contains(result));

            return result;
        }
    }

    /// <inheritdoc/>
    public override IHeapHandle<TVal, TPr> Add(TVal value, TPr priority)
    {
        // Preconditions
        if (priority is null) throw new ArgumentNullException(nameof(priority), ErrorMessages.NullPriority);

        var handle = new ArrayHandle(value, priority);
        Add(handle);
        var result = handle;

        // Postconditions
        Debug.Assert(result != null);
        Debug.Assert(Contains(result));
        Debug.Assert(EqualityComparer.Equals(result.Value, value));
        Debug.Assert(Comparer.Compare(result.Priority, priority) == 0);

        return result;
    }

    /// <inheritdoc/>
    public override void Clear()
    {
        Count = 0;
        _handles = new ArrayHandle[MinSize];
    }

    /// <inheritdoc/>
    public override void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    /// <inheritdoc/>
    public override IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        for (var i = MinIndex; i < Count; ++i)
        {
            yield return _handles[i];
        }
    }

    /// <inheritdoc/>
    [SuppressMessage("Major Code Smell", "S2589:Boolean expressions should not be gratuitous", Justification = "Count property in while loop is decreased by RemoveMin call")]
    public override void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        if (other is ArrayHeap<TVal, TPr> otherArrayHeap)
        {
            var otherHandles = otherArrayHeap._handles;
            for (var i = MinIndex; i < otherArrayHeap.Count; ++i)
            {
                Add(otherHandles[i]);
            }
            otherArrayHeap.Clear();
            return;
        }
        if (other is ArrayHeap<TVal2, TPr2> otherArrayHeap2)
        {
            var other2Handles = otherArrayHeap2._handles;
            for (var i = MinIndex; i < otherArrayHeap2.Count; ++i)
            {
                Add(ArrayHandle.New(other2Handles[i]));
            }
            otherArrayHeap2.Clear();
            return;
        }
        while (other.Count > 0)
        {
            Add(ArrayHandle.New(other.RemoveMin()));
        }
    }

    /// <inheritdoc/>
    public override bool Remove(IHeapHandle<TVal, TPr> item)
    {
        var arrayHandle = GetHandle(item as ArrayHandle);
        if (arrayHandle != null)
        {
            RemoveAt(arrayHandle.Index);
            return true;
        }
        return false;
    }

    /// <inheritdoc/>
    public override IHeapHandle<TVal, TPr> RemoveMin()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

        var min = Min;
        RemoveAt(MinIndex);
        return min;
    }

    /// <inheritdoc/>
    public override IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        if (Count == 0)
        {
            yield break;
        }
        var queue = new Queue<KeyValuePair<ArrayHandle, ReadOnlyTree<TVal, TPr>>>();
        queue.Enqueue(new KeyValuePair<ArrayHandle, ReadOnlyTree<TVal, TPr>>(_handles[MinIndex], (ReadOnlyTree<TVal, TPr>)null));
        ReadOnlyTree<TVal, TPr> root = null;
        while (queue.Count > 0)
        {
            var vi = queue.Dequeue();
            var it = vi.Key;
            var t = new ReadOnlyTree<TVal, TPr>(it.Value, it.Priority, vi.Value);
            if (it.Index == MinIndex)
            {
                root = t;
            }
            var start = (_childCount * it.Index) + 1;
            for (var i = 0; i < _childCount && i + start < Count; ++i)
            {
                queue.Enqueue(new KeyValuePair<ArrayHandle, ReadOnlyTree<TVal, TPr>>(_handles[start + i], t));
            }
        }
        yield return root;
    }

    /// <inheritdoc/>
    public override string ToString()
    {
        return this.CommonToString();
    }

    /// <inheritdoc/>
    protected override ArrayHandle GetHandle(ArrayHandle handle)
    {
        if (handle == null)
        {
            return null;
        }
        var i = handle.Index;
        if (i >= Count || !ReferenceEquals(handle, _handles[i]))
        {
            return null;
        }
        return handle;
    }

    /// <inheritdoc/>
    [SuppressMessage("Major Code Smell", "S127:\"for\" loop stop conditions should be invariant")]
    protected override void MoveDown(ArrayHandle handle)
    {
        var i = handle.Index;

        for (int j = (_childCount * i) + 1, k = j + _childCount; j < Count; j = (_childCount * i) + 1, k = j + _childCount)
        {
            var mc = j; // Min child index
            for (++j; j < Count && j < k; ++j)
            {
                if (Compare(_handles[j].Priority, _handles[mc].Priority) < 0)
                {
                    mc = j;
                }
            }
            if (Compare(handle.Priority, _handles[mc].Priority) <= 0)
            {
                break;
            }
            PlaceAt(_handles[mc], i);
            i = mc;
        }
        PlaceAt(handle, i);
    }

    /// <inheritdoc/>
    protected override void MoveUp(ArrayHandle handle)
    {
        var i = handle.Index;
        while (i != MinIndex)
        {
            var j = (i - 1) / _childCount;
            var parent = _handles[j];
            if (Compare(handle.Priority, parent.Priority) >= 0)
            {
                break;
            }
            PlaceAt(parent, i);
            i = j;
        }
        PlaceAt(handle, i);
    }

    private void Add(ArrayHandle handle)
    {
        if (Count == _handles.Length)
        {
            Array.Resize(ref _handles, Count * ResizeFactor);
        }
        PlaceAt(handle, Count++);
        MoveUp(handle);
        CheckHandlesSize();
    }

    [Conditional("DEBUG")]
    private void CheckHandlesSize()
    {
        if (Count <= MinSize)
        {
            Debug.Assert(_handles.Length == MinSize);
            return;
        }
        var expectedLog = Math.Log(_handles.Length, ResizeFactor);
        var foundLog = Math.Ceiling(Math.Log(Count, ResizeFactor));
        Debug.Assert(expectedLog.Equals(foundLog));
    }

    private void PlaceAt(ArrayHandle handle, int index)
    {
        Debug.Assert(index <= Count);
        handle.Index = index;
        _handles[index] = handle;
    }

    private void RemoveAt(int index)
    {
        var last = _handles[--Count];
        // If we have an empty heap, or we are deleting the last element, we should not swap any
        // element. In the first case, because there is nothing to swap; in the second case,
        // because it is useless and the capacity of the array might have gotten too little.
        if (Count != 0 && Count != index)
        {
            last.Index = index;
            _handles[index] = last;
            MoveDown(last);
        }
        if (Count >= MinSize && Count * ResizeFactor == _handles.Length)
        {
            Array.Resize(ref _handles, Count);
        }
        CheckHandlesSize();
    }

    public sealed class ArrayHandle : IHeapHandle<TVal, TPr>, IItem
    {
        public ArrayHandle(TVal value, TPr priority)
        {
            Value = value;
            Priority = priority;
        }

        /// <inheritdoc/>
        public int Index { get; set; }

        /// <inheritdoc/>
        public TPr Priority { get; set; }

        /// <inheritdoc/>
        public TVal Value { get; set; }

        public static ArrayHandle New<TVal2, TPr2>(IHeapHandle<TVal2, TPr2> handle)
            where TVal2 : TVal
            where TPr2 : TPr
        {
            return new ArrayHandle(handle.Value, handle.Priority);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format("[Value: {0}; Priority: {1}]", Value, Priority);
        }
    }
}
