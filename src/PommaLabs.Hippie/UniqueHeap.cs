﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core;

namespace PommaLabs.Hippie;

public sealed class UniqueHeap<TVal, TPr> : IHeap<TVal, TPr>
{
    private readonly Func<TPr, TPr, int> _comparer;
    private readonly Dictionary<TVal, IHeapHandle<TVal, TPr>> _map;
    private readonly IRawHeap<TVal, TPr> _wrappedHeap;

    internal UniqueHeap(IRawHeap<TVal, TPr> heapToWrap, IEqualityComparer<TVal> equalityComparer)
    {
        _comparer = heapToWrap.Comparer.Compare;
        EqualityComparer = equalityComparer ?? EqualityComparer<TVal>.Default;
        _wrappedHeap = heapToWrap;
        _map = new Dictionary<TVal, IHeapHandle<TVal, TPr>>(EqualityComparer);
    }

    public IComparer<TPr> Comparer
    {
        get { return _wrappedHeap.Comparer; }
    }

    public int Count
    {
        get { return _wrappedHeap.Count; }
    }

    public IEqualityComparer<TVal> EqualityComparer { get; }

    public bool IsReadOnly
    {
        get { return false; }
    }

    public IHeapHandle<TVal, TPr> Min
    {
        get { return _wrappedHeap.Min; }
    }

    public TPr this[TVal val]
    {
        get
        {
            // Preconditions
            if (!Contains(val)) throw new ArgumentException(ErrorMessages.NoValue, nameof(val));

            var result = _map[val].Priority;

            // Postconditions
            Debug.Assert(result is not null);
            Debug.Assert(Contains(val, result));

            return result;
        }
        set
        {
            // Preconditions
            if (value is null) throw new ArgumentNullException(nameof(value), ErrorMessages.NullPriority);
            if (!Contains(val)) throw new ArgumentException(ErrorMessages.NoValue, nameof(val));

            _wrappedHeap.UpdatePriorityOf(_map[val], value);

            // Postconditions
            Debug.Assert(Contains(val, value));
            Debug.Assert(Comparer.Compare(this[val], value) == 0);
        }
    }

    public void Add(TVal value, TPr priority)
    {
        _map.Add(value, _wrappedHeap.Add(value, priority));
    }

    public void Add(IHeapHandle<TVal, TPr> handle)
    {
        _map.Add(handle.Value, _wrappedHeap.Add(handle.Value, handle.Priority));
    }

    public void Clear()
    {
        _wrappedHeap.Clear();
        _map.Clear();
    }

    public bool Contains(TVal value)
    {
        // Preconditions
        if (value is null) throw new ArgumentNullException(nameof(value), ErrorMessages.NullValue);

        return _map.ContainsKey(value);
    }

    public bool Contains(TVal value, TPr priority)
    {
        // Preconditions
        if (value is null) throw new ArgumentNullException(nameof(value), ErrorMessages.NullValue);

        if (priority is null)
        {
            // There should be no handle with null priority.
            return false;
        }
        if (_map.TryGetValue(value, out var handle))
        {
            return _comparer(handle.Priority, priority) == 0;
        }
        return false;
    }

    public bool Contains(IHeapHandle<TVal, TPr> handle)
    {
        if (handle is null || handle.Priority is null)
        {
            // Handle should not be null and there should be no handle with null priority.
            return false;
        }
        if (_map.TryGetValue(handle.Value, out var tmp))
        {
            return _comparer(handle.Priority, tmp.Priority) == 0;
        }
        return false;
    }

    public void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        _wrappedHeap.CopyTo(array, arrayIndex);
    }

    public IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        return _wrappedHeap.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _wrappedHeap.GetEnumerator();
    }

    public void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
        where TVal2 : TVal
        where TPr2 : TPr
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        // Handles are cached to avoid enumerating other heap two times (it may be very costly,
        // in particular for tree heaps).
        var tmp = new List<IHeapHandle<TVal2, TPr2>>(other.Count);
        foreach (var h in other)
        {
            if (_map.ContainsKey(h.Value))
            {
                throw new ArgumentException(ErrorMessages.MergeConflict);
            }
            tmp.Add(h);
        }
        foreach (var h in tmp)
        {
            _map.Add(h.Value, _wrappedHeap.Add(h.Value, h.Priority));
        }
        other.Clear();
    }

    public TPr PriorityOf(TVal value)
    {
        // Preconditions
        if (!Contains(value)) throw new ArgumentException(ErrorMessages.NoValue, nameof(value));

        var result = _map[value].Priority;

        // Postconditions
        Debug.Assert(result is not null);
        Debug.Assert(Contains(value, result));

        return result;
    }

    public IHeapHandle<TVal, TPr> Remove(TVal value)
    {
        // Preconditions
        if (!Contains(value)) throw new ArgumentException(ErrorMessages.NoValue, nameof(value));

        var result = _map[value];
        _map.Remove(value);
        var hasRemoved = _wrappedHeap.Remove(result);

        // Postconditions
        Debug.Assert(hasRemoved);
        Debug.Assert(result != null);
        Debug.Assert(!Contains(value) && !Contains(value, result.Priority));

        return result;
    }

    public bool Remove(IHeapHandle<TVal, TPr> handle)
    {
        if (_map.TryGetValue(handle.Value, out var tmp))
        {
            if (_comparer(handle.Priority, tmp.Priority) == 0)
            {
                _map.Remove(handle.Value);
                var hasRemoved = _wrappedHeap.Remove(tmp);
                Debug.Assert(hasRemoved);
                return true;
            }
            return false;
        }
        return false;
    }

    public IHeapHandle<TVal, TPr> RemoveMin()
    {
        var min = _wrappedHeap.RemoveMin();
        _map.Remove(min.Value);
        return min;
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        return _wrappedHeap.CommonToForest<TVal, TPr, TVal, TPr>((tree, parent) => TransformTree(tree, parent));
    }

    public override string ToString()
    {
        return _wrappedHeap.ToString();
    }

    public TPr Update(TVal value, TVal newValue, TPr newPriority)
    {
        // Preconditions
        if (newValue is null) throw new ArgumentNullException(nameof(newValue), ErrorMessages.NullValue);
        if (!EqualityComparer.Equals(value, newValue) && Contains(newValue)) throw new ArgumentException(ErrorMessages.ValueContained, nameof(newValue));

        var oldPriority = UpdatePriorityOf(value, newPriority);
        UpdateValue(value, newValue);
        var result = oldPriority;

        // Postconditions
        Debug.Assert(result is not null);
        Debug.Assert(Contains(newValue) && Contains(newValue, newPriority));
        Debug.Assert(EqualityComparer.Equals(value, newValue) || !Contains(value));
        Debug.Assert(EqualityComparer.Equals(value, newValue) || !Contains(value, result));

        return result;
    }

    public TPr UpdatePriorityOf(TVal value, TPr newPriority)
    {
        // Preconditions
        if (newPriority is null) throw new ArgumentNullException(nameof(newPriority), ErrorMessages.NullPriority);
        if (!Contains(value)) throw new ArgumentException(ErrorMessages.NoValue, nameof(value));

        var result = _wrappedHeap.UpdatePriorityOf(_map[value], newPriority);

        // Postconditions
        Debug.Assert(Contains(value, newPriority));
        Debug.Assert(Comparer.Compare(this[value], newPriority) == 0);
        Debug.Assert(Comparer.Compare(result, newPriority) == 0 || !Contains(value, result));

        return result;
    }

    public void UpdateValue(TVal value, TVal newValue)
    {
        // Preconditions
        if (newValue is null) throw new ArgumentNullException(nameof(newValue), ErrorMessages.NullValue);
        if (!Contains(value)) throw new ArgumentException(ErrorMessages.NoValue, nameof(value));
        if (!EqualityComparer.Equals(value, newValue) && Contains(newValue)) throw new ArgumentException(ErrorMessages.ValueContained, nameof(newValue));

        if (EqualityComparer.Equals(value, newValue))
        {
            return;
        }
        var handle = _map[value];
        _wrappedHeap.UpdateValue(handle, newValue);
        _map.Remove(value);
        _map.Add(newValue, handle);

        // Postconditions
        Debug.Assert(Contains(newValue));
        Debug.Assert(EqualityComparer.Equals(value, newValue) || !Contains(value));
    }

    private static ReadOnlyTree<TVal, TPr> TransformTree(IReadOnlyTree<TVal, TPr> tree, ReadOnlyTree<TVal, TPr> parent)
    {
        return new ReadOnlyTree<TVal, TPr>(tree.Value, tree.Priority, parent);
    }
}
