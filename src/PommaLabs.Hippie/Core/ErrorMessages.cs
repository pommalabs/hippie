﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Hippie.Core;

internal static class ErrorMessages
{
    public const string ContainedItem = "TODO";
    public const string DifferentComparers = "Merge is not allowed when heaps have not the same comparer.";
    public const string EmptyList = "List is empty";
    public const string EmptyQueue = "Queue is empty";
    public const string EmptyStack = "Stack is empty";
    public const string HandleContained = "Heap already contains given handle.";
    public const string InefficientMethod = "Implementation of this method would be inefficient.";
    public const string InvalidHandle = "Handle is not valid.";
    public const string MergeConflict = "Some values are already held in the heap.";
    public const string NoHandle = "Heap does not contain given handle.";
    public const string NoMin = "Heap is empty, there is no minimum.";
    public const string NotContainedItem = "TODO";
    public const string NoValue = "Heap does not contain given value.";
    public const string NullComparer = "Comparer cannot be null.";
    public const string NullHandle = "Handle cannot be null.";
    public const string NullList = "TODO";
    public const string NullOther = "Other heap cannot be null.";
    public const string NullPriority = "Priority cannot be null, in order to avoid issues with comparers.";
    public const string NullValue = "Value cannot be null, in order to avoid issues with equality comparers.";
    public const string ValueContained = "Heap already contains given value.";

    public const string WrongChildCount = "Child count must be greater than or equal to two.";
}
