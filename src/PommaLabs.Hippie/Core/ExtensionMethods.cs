﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;

namespace PommaLabs.Hippie.Core;

internal static class ExtensionMethods
{
    public static void CommonCopyTo<TEn, TArr>(this ICollection<TEn> collection, TArr[] array, int idx,
                                               Func<TEn, TArr> transform)
    {
        var enumerator = collection.GetEnumerator();
        var lastIndex = collection.Count + idx;
        for (var i = idx; i < lastIndex; ++i)
        {
            enumerator.MoveNext();
            array[i] = transform(enumerator.Current);
        }
    }

    public static void CommonMerge<TV1, TP1, TV2, TP2>(this IThinHeap<TV1, TP1> heap, IThinHeap<TV2, TP2> other)
        where TV2 : TV1
        where TP2 : TP1
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(heap.Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(heap, other) || other.Count == 0)
        {
            return;
        }
        foreach (var handle in other)
        {
            heap.Add(handle.Value, handle.Priority);
        }
        other.Clear();
    }

    public static IEnumerable<IReadOnlyTree<TV2, TP2>> CommonToForest<TV1, TP1, TV2, TP2>(
        this IThinHeap<TV1, TP1> heap,
        Func<IReadOnlyTree<TV1, TP1>, ReadOnlyTree<TV2, TP2>, ReadOnlyTree<TV2, TP2>> transform)
    {
        foreach (var t in heap.ToReadOnlyForest())
        {
            var tt = t.BreadthFirstVisit(transform, null);
            yield return tt.ToList()[0];
        }
    }

    public static string CommonToString<TV, TP>(this IThinHeap<TV, TP> heap)
    {
        return (heap.Count == 0) ? "[Count: 0]" : string.Format("[Count: {0}; Min: {1}]", heap.Count, heap.Min);
    }
}
