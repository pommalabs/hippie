﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Hippie.Core.LinkedLists.Core;

public sealed class DoublyNode<T> : NodeBase<DoublyNode<T>, T>
{
    internal DoublyNode(T item, DoublyNode<T> next, DoublyNode<T> prev)
        : base(item, next)
    {
        Prev = prev;
    }

    public DoublyNode<T> Prev { get; set; }

    public override string ToString()
    {
        return $"{nameof(Prev)}: {Prev?.ToString()} --> {nameof(Item)}: {Item?.ToString()} --> {nameof(Next)}: {Next?.ToString()}";
    }
}

public abstract class NodeBase<TN, TI> where TN : NodeBase<TN, TI>
{
    private protected NodeBase(TI item, TN next)
    {
        Item = item;
        Next = next;
    }

    public TI Item { get; }

    public TN Next { get; set; }
}

public sealed class SinglyNode<T> : NodeBase<SinglyNode<T>, T>
{
    internal SinglyNode(T item, SinglyNode<T> next)
        : base(item, next)
    {
    }

    public override string ToString()
    {
        return $"{nameof(Item)}: {Item?.ToString()} --> {nameof(Next)}: {Next?.ToString()}";
    }
}
