﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the list will contain.</typeparam>
public sealed class ThinLinkedList<T> : ThinListBase<SinglyNode<T>, T>
{
    #region Construction

    /// <summary>
    ///   Returns the default implementation of the <see cref="IThinLinkedList{TItem}"/> interface.
    /// </summary>
    /// <returns>An implementation of the <see cref="IThinLinkedList{TItem}"/> interface.</returns>
    public ThinLinkedList()
        : base(EqualityComparer<T>.Default)
    {
    }

    /// <summary>
    ///   Returns the default implementation of the <see cref="IThinLinkedList{TItem}"/>
    ///   interface, using specified equality comparer.
    /// </summary>
    /// <param name="equalityComparer">
    ///   The equality comparer that it will be used to determine whether two items are equal.
    /// </param>
    /// <returns>
    ///   An implementation of the <see cref="IThinLinkedList{TItem}"/> interface using
    ///   specified equality comparer.
    /// </returns>
    public ThinLinkedList(IEqualityComparer<T> equalityComparer)
        : base(equalityComparer)
    {
    }

    #endregion Construction

    #region ICollection Members

    /// <inheritdoc/>
    public override void Add(T item)
    {
        AddFirst(item);
    }

    /// <inheritdoc/>
    public override void Clear()
    {
        FirstNode = null;
        Count = 0;
    }

    #endregion ICollection Members

    #region IThinLinkedList Members

    /// <inheritdoc/>
    public override void AddFirst(T item)
    {
        FirstNode = new SinglyNode<T>(item, FirstNode);
        Count++;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(First, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override T RemoveFirst()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var first = FirstNode.Item;
        FirstNode = FirstNode.Next;
        Count--;
        return first;
    }

    #endregion IThinLinkedList Members
}
