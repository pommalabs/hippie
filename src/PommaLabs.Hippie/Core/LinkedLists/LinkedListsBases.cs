﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace PommaLabs.Hippie.Core.LinkedLists.Core;

public abstract class ListBase<TN, TI> : ThinListBase<TN, TI>, ILinkedList<TI>
    where TN : NodeBase<TN, TI>
{
    private protected ListBase(IEqualityComparer<TI> equalityComparer)
        : base(equalityComparer)
    {
    }

    protected TN LastNode { get; set; }

    #region ICollection Members

    /// <inheritdoc/>
    public override void Clear()
    {
        FirstNode = LastNode = null;
        Count = 0;
    }

    #endregion ICollection Members

    #region IThinLinkedList Members

    private protected override void UnlinkNode(TN prev, TN node)
    {
        prev!.Next = node.Next;
        if (node == LastNode)
        {
            LastNode = prev;
        }
    }

    #endregion

    #region ILinkedList Members

    /// <inheritdoc/>
    public TI Last
    {
        get
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

            return LastNode.Item;
        }
    }

    /// <inheritdoc/>
    public abstract void AddLast(TI item);

    /// <inheritdoc/>
    public virtual void Append(ILinkedList<TI> list)
    {
        // Preconditions
        if (list == null) throw new ArgumentNullException(nameof(list), ErrorMessages.NullList);

        if (list.Count == 0)
        {
            return;
        }
        foreach (var i in list)
        {
            AddLast(i);
        }
        list.Clear();

        // Postconditions
        Debug.Assert(list.Count == 0);
    }

    #endregion ILinkedList Members
}

public abstract class ThinListBase<TN, TI> : IThinLinkedList<TI>
    where TN : NodeBase<TN, TI>
{
    private protected ThinListBase(IEqualityComparer<TI> equalityComparer)
    {
        EqualityComparer = equalityComparer ?? throw new ArgumentNullException(nameof(equalityComparer));

        // Postconditions
        Debug.Assert(Count == 0);
        Debug.Assert(ReferenceEquals(EqualityComparer, equalityComparer));
    }

    protected TN FirstNode { get; set; }

    #region IEnumerable Members

    /// <inheritdoc/>
    public IEnumerator<TI> GetEnumerator()
    {
        for (var n = FirstNode; n != null; n = n.Next)
        {
            yield return n.Item;
        }
    }

    /// <inheritdoc/>
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    #endregion IEnumerable Members

    #region ICollection Members

    /// <inheritdoc/>
    public int Count { get; protected set; }

    /// <inheritdoc/>
    public bool IsReadOnly => false;

    /// <inheritdoc/>
    public abstract void Add(TI item);

    /// <inheritdoc/>
    public abstract void Clear();

    /// <inheritdoc/>
    public virtual bool Contains(TI item)
    {
        if (Count == 0)
        {
            return false;
        }
        if (Count == 1)
        {
            return EqualityComparer.Equals(FirstNode.Item, item);
        }
        for (var n = FirstNode; n != null; n = n.Next)
        {
            if (EqualityComparer.Equals(n.Item, item))
            {
                return true;
            }
        }
        return false;
    }

    /// <inheritdoc/>
    public void CopyTo(TI[] array, int arrayIndex)
    {
        var listEn = GetEnumerator();
        for (var i = 0; i < Count; ++i)
        {
            listEn.MoveNext();
            array[arrayIndex + i] = listEn.Current;
        }
    }

    #endregion ICollection Members

    #region IThinLinkedList Members

    /// <inheritdoc/>
    public IEqualityComparer<TI> EqualityComparer { get; }

    /// <inheritdoc/>
    public TI First
    {
        get
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

            return FirstNode.Item;
        }
    }

    /// <inheritdoc/>
    public abstract void AddFirst(TI item);

    /// <inheritdoc/>
    public abstract TI RemoveFirst();

    /// <inheritdoc/>
    public virtual bool Remove(TI item)
    {
        TN prev = null;
        TN node = null;
        for (var n = FirstNode; n != null; prev = n, n = n.Next)
        {
            if (!EqualityComparer.Equals(n.Item, item))
            {
                continue;
            }
            node = n;
            break;
        }
        if (node == null)
        {
            // Node is not contained inside the list.
            return false;
        }

        if (node == FirstNode)
        {
            RemoveFirst();
        }
        else
        {
            UnlinkNode(prev, node);
            Count--;
        }
        return true;
    }

    private protected virtual void UnlinkNode(TN prev, TN node)
    {
        prev.Next = node.Next;
    }

    #endregion IThinLinkedList Members
}
