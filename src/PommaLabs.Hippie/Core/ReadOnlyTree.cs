﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;

namespace PommaLabs.Hippie.Core;

internal sealed class ReadOnlyTree<TVal, TPr> : IReadOnlyTree<TVal, TPr>
{
    private readonly List<ReadOnlyTree<TVal, TPr>> _children = new();
    private readonly ReadOnlyTree<TVal, TPr> _parent;

    public ReadOnlyTree(TVal value, TPr priority, ReadOnlyTree<TVal, TPr> parent)
    {
        Value = value;
        Priority = priority;
        _parent = parent;
        if (parent != null)
        {
            parent._children.Add(this);
        }
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> Children
    {
        get { return _children; }
    }

    public IReadOnlyTree<TVal, TPr> Parent
    {
        get { return _parent; }
    }

    public TPr Priority { get; }

    public TVal Value { get; }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> BreadthFirstVisit()
    {
        var queue = new Queue<ReadOnlyTree<TVal, TPr>>();
        queue.Enqueue(this);
        while (queue.Count > 0)
        {
            var t = queue.Dequeue();
            yield return t;
            foreach (var c in t._children)
            {
                queue.Enqueue(c);
            }
        }
    }

    public void BreadthFirstVisit(Action<IReadOnlyTree<TVal, TPr>> visitor)
    {
        var queue = new Queue<ReadOnlyTree<TVal, TPr>>();
        queue.Enqueue(this);
        while (queue.Count > 0)
        {
            var t = queue.Dequeue();
            visitor(t);
            foreach (var c in t._children)
            {
                queue.Enqueue(c);
            }
        }
    }

    public IEnumerable<TRet> BreadthFirstVisit<TRet>(Func<IReadOnlyTree<TVal, TPr>, TRet, TRet> visitor, TRet start)
    {
        var queue = new Queue<KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>>();
        queue.Enqueue(new KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>(this, start));
        while (queue.Count > 0)
        {
            var tuple = queue.Dequeue();
            var res = visitor(tuple.Key, tuple.Value);
            yield return res;
            foreach (var c in tuple.Key._children)
            {
                queue.Enqueue(new KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>(c, res));
            }
        }
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> DepthFirstVisit()
    {
        var stack = new Stack<ReadOnlyTree<TVal, TPr>>();
        stack.Push(this);
        while (stack.Count > 0)
        {
            var t = stack.Pop();
            yield return t;
            foreach (var c in t._children)
            {
                stack.Push(c);
            }
        }
    }

    public void DepthFirstVisit(Action<IReadOnlyTree<TVal, TPr>> visitor)
    {
        var stack = new Stack<ReadOnlyTree<TVal, TPr>>();
        stack.Push(this);
        while (stack.Count > 0)
        {
            var t = stack.Pop();
            visitor(t);
            foreach (var c in t._children)
            {
                stack.Push(c);
            }
        }
    }

    public IEnumerable<TRet> DepthFirstVisit<TRet>(Func<IReadOnlyTree<TVal, TPr>, TRet, TRet> visitor, TRet start)
    {
        var stack = new Stack<KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>>();
        stack.Push(new KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>(this, start));
        while (stack.Count > 0)
        {
            var tuple = stack.Pop();
            var res = visitor(tuple.Key, tuple.Value);
            yield return res;
            foreach (var c in tuple.Key._children)
            {
                stack.Push(new KeyValuePair<ReadOnlyTree<TVal, TPr>, TRet>(c, res));
            }
        }
    }

    public override string ToString()
    {
        return string.Format("[Value: {0}; Priority: {1}; Children: {2}]", Value, Priority, Children.Count());
    }
}

internal sealed class ReadOnlyTree<T> : IReadOnlyTree<T>
{
    private readonly List<ReadOnlyTree<T>> _children = new();
    private readonly ReadOnlyTree<T> _parent;

    public ReadOnlyTree(T item, ReadOnlyTree<T> parent)
    {
        Item = item;
        _parent = parent;
        if (parent != null)
        {
            parent._children.Add(this);
        }
    }

    public IEnumerable<IReadOnlyTree<T>> Children
    {
        get { return _children; }
    }

    public T Item { get; }

    public IReadOnlyTree<T> Parent
    {
        get { return _parent; }
    }

    public IEnumerable<IReadOnlyTree<T>> BreadthFirstVisit()
    {
        var queue = new Queue<ReadOnlyTree<T>>();
        queue.Enqueue(this);
        while (queue.Count > 0)
        {
            var t = queue.Dequeue();
            yield return t;
            foreach (var c in t._children)
            {
                queue.Enqueue(c);
            }
        }
    }

    public void BreadthFirstVisit(Action<IReadOnlyTree<T>> visitor)
    {
        var queue = new Queue<ReadOnlyTree<T>>();
        queue.Enqueue(this);
        while (queue.Count > 0)
        {
            var t = queue.Dequeue();
            visitor(t);
            foreach (var c in t._children)
            {
                queue.Enqueue(c);
            }
        }
    }

    public IEnumerable<TRet> BreadthFirstVisit<TRet>(Func<IReadOnlyTree<T>, TRet, TRet> visitor, TRet start)
    {
        var queue = new Queue<KeyValuePair<ReadOnlyTree<T>, TRet>>();
        queue.Enqueue(new KeyValuePair<ReadOnlyTree<T>, TRet>(this, start));
        while (queue.Count > 0)
        {
            var tuple = queue.Dequeue();
            var res = visitor(tuple.Key, tuple.Value);
            yield return res;
            foreach (var c in tuple.Key._children)
            {
                queue.Enqueue(new KeyValuePair<ReadOnlyTree<T>, TRet>(c, res));
            }
        }
    }

    public IEnumerable<IReadOnlyTree<T>> DepthFirstVisit()
    {
        var stack = new Stack<ReadOnlyTree<T>>();
        stack.Push(this);
        while (stack.Count > 0)
        {
            var t = stack.Pop();
            yield return t;
            foreach (var c in t._children)
            {
                stack.Push(c);
            }
        }
    }

    public void DepthFirstVisit(Action<IReadOnlyTree<T>> visitor)
    {
        var stack = new Stack<ReadOnlyTree<T>>();
        stack.Push(this);
        while (stack.Count > 0)
        {
            var t = stack.Pop();
            visitor(t);
            foreach (var c in t._children)
            {
                stack.Push(c);
            }
        }
    }

    public IEnumerable<TRet> DepthFirstVisit<TRet>(Func<IReadOnlyTree<T>, TRet, TRet> visitor, TRet start)
    {
        var stack = new Stack<KeyValuePair<ReadOnlyTree<T>, TRet>>();
        stack.Push(new KeyValuePair<ReadOnlyTree<T>, TRet>(this, start));
        while (stack.Count > 0)
        {
            var tuple = stack.Pop();
            var res = visitor(tuple.Key, tuple.Value);
            yield return res;
            foreach (var c in tuple.Key._children)
            {
                stack.Push(new KeyValuePair<ReadOnlyTree<T>, TRet>(c, res));
            }
        }
    }

    public override string ToString()
    {
        return string.Format("[Item: {0}; Children: {1}]", Item, Children.Count());
    }
}
