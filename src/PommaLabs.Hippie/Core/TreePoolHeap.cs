﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.Core;

public abstract class TreePoolHeap<TVal, TPr> : TreeHeap<TVal, TPr>
{
    private protected TreePoolHeap(IComparer<TPr> comparer)
        : base(comparer)
    {
    }

    private protected SinglyLinkedList<Tree> TreePool { get; } = new();

    public override IHeapHandle<TVal, TPr> Add(TVal value, TPr priority)
    {
        // Preconditions
        if (priority is null) throw new ArgumentNullException(nameof(priority), ErrorMessages.NullPriority);

        var tree = new Tree(value, priority, Version);
        TreePool.AddLast(tree);
        FixMin(tree);
        Count++;
        var result = tree.Handle;

        // Postconditions
        Debug.Assert(result != null);
        Debug.Assert(Contains(result));
        Debug.Assert(EqualityComparer.Equals(result.Value, value));
        Debug.Assert(Comparer.Compare(result.Priority, priority) == 0);

        return result;
    }

    public override void Clear()
    {
        base.Clear();
        TreePool.Clear();
    }

    private protected abstract void FixMin(Tree tree);
}
