﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.Core;

public abstract class TreeHeap<TVal, TPr> : RawHeap<TVal, TPr, TreeHeap<TVal, TPr>.TreeHandle>
{
    private protected TreeHeap(IComparer<TPr> comparer)
        : base(comparer)
    {
    }

    public override IHeapHandle<TVal, TPr> Min
    {
        get
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

            var result = MinTree.Handle;

            // Postconditions
            Debug.Assert(Contains(result));

            return result;
        }
    }

    internal Tree MinTree { get; set; }

    /// <summary>
    ///   Used to give a "version" to each node. When the heap is cleared, the version is
    ///   updated, so that all nodes, even if they have a valid hint (reference to node), are
    ///   marked as invalid because of the old version they have.
    /// </summary>
    protected TreeVersion Version { get; set; } = new();

    public override void Clear()
    {
        Version = new TreeVersion();
        Count = 0;
        MinTree = null;
    }

    internal Tree Meld(Tree t1, Tree t2)
    {
        // It is a mistake to meld the same trees.
        Debug.Assert(!ReferenceEquals(t1, t2));

        if (Compare(t1.Priority, t2.Priority) < 0)
        {
            t2.Parent = t1;
            t1.Children.AddLast(t2);
            return t1;
        }
        t1.Parent = t2;
        t2.Children.AddLast(t1);
        return t2;
    }

    internal Tree NullMeld(Tree t1, Tree t2)
    {
        if (t1 == null)
        {
            return t2;
        }
        if (t2 == null)
        {
            return t1;
        }
        return Meld(t1, t2);
    }

    protected sealed override TreeHandle GetHandle(TreeHandle handle)
    {
        return (handle == null || handle.Version == null || handle.Version.Id != Version.Id) ? null : handle;
    }

    public sealed class TreeHandle : IHeapHandle<TVal, TPr>, IItem
    {
        internal TreeHandle(TVal value, TPr priority, TreeVersion version, Tree tree)
        {
            Value = value;
            Priority = priority;
            Version = version;
            Tree = tree;
        }

        public TPr Priority { get; set; }

        public TVal Value { get; set; }

        internal Tree Tree { get; set; }

        internal TreeVersion Version { get; set; }

        public override string ToString()
        {
            return string.Format("[Value: {0}; Priority: {1}]", Value, Priority);
        }
    }

    public sealed class TreeVersion
    {
        public object Id { get; set; } = new();

        public override string ToString()
        {
            return string.Format("[Id: {0}]", Id);
        }
    }

    internal sealed class Tree
    {
        public Tree(TVal value, TPr priority, TreeVersion version)
        {
            Handle = new TreeHandle(value, priority, version, this);
        }

        public SinglyLinkedList<Tree> Children { get; } = new();

        public TreeHandle Handle { get; set; }

        /// <summary>
        ///   Used by Fibonacci heap.
        /// </summary>
        public short Mark { get; set; }

        public Tree Parent { get; set; }

        public TPr Priority
        {
            get { return Handle.Priority; }
        }

        public IEnumerable<Tree> BreadthFirstVisit()
        {
            var queue = new Queue<Tree>();
            queue.Enqueue(this);
            while (queue.Count != 0)
            {
                var tree = queue.Dequeue();
                yield return tree;
                foreach (var child in tree.Children)
                {
                    queue.Enqueue(child);
                }
            }
        }

        public Tree MinChild(Func<TPr, TPr, int> comparer)
        {
            if (Children.Count == 0)
            {
                return null;
            }
            var min = Children.First;
            foreach (var child in Children)
            {
                if (comparer(child.Priority, min.Priority) < 0)
                {
                    min = child;
                }
            }
            return min;
        }

        public void SwapRootWith(Tree other)
        {
            var tmp = other.Handle;
            Handle.Tree = other;
            other.Handle = Handle;
            tmp.Tree = this;
            Handle = tmp;
        }

        public IReadOnlyTree<TVal, TPr> ToReadOnlyTree()
        {
            var queue = new Queue<KeyValuePair<Tree, ReadOnlyTree<TVal, TPr>>>();
            queue.Enqueue(new KeyValuePair<Tree, ReadOnlyTree<TVal, TPr>>(this, (ReadOnlyTree<TVal, TPr>)null));
            ReadOnlyTree<TVal, TPr> root = null;
            while (queue.Count > 0)
            {
                var vi = queue.Dequeue();
                var it = vi.Key;
                var t = new ReadOnlyTree<TVal, TPr>(it.Handle.Value, it.Priority, vi.Value);
                if (root == null)
                {
                    root = t;
                }
                foreach (var c in it.Children)
                {
                    queue.Enqueue(new KeyValuePair<Tree, ReadOnlyTree<TVal, TPr>>(c, t));
                }
            }
            return root;
        }

        public override string ToString()
        {
            return string.Format("[Item: {0}; Children: {1}]", base.ToString(), Children);
        }
    }
}
