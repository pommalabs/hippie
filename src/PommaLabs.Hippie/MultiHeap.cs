﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PommaLabs.Hippie.Core;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie;

public sealed class MultiHeap<TVal> : IHeap<TVal>
{
    private readonly Dictionary<TVal, LinkedStack<IHeapHandle<TVal, TVal>>> _map;
    private readonly IRawHeap<TVal, TVal> _wrappedHeap;

    internal MultiHeap(IRawHeap<TVal, TVal> heapToWrap, IEqualityComparer<TVal> equalityComparer)
    {
        _wrappedHeap = heapToWrap;
        EqualityComparer = equalityComparer ?? EqualityComparer<TVal>.Default;
        _map = new Dictionary<TVal, LinkedStack<IHeapHandle<TVal, TVal>>>(EqualityComparer);
    }

    public IComparer<TVal> Comparer
    {
        get { return _wrappedHeap.Comparer; }
    }

    public int Count
    {
        get { return _wrappedHeap.Count; }
    }

    public IEqualityComparer<TVal> EqualityComparer { get; }

    public bool IsReadOnly
    {
        get { return false; }
    }

    public TVal Min
    {
        get { return _wrappedHeap.Min.Value; }
    }

    public void Add(TVal item)
    {
        var handle = _wrappedHeap.Add(item, item);
        if (_map.TryGetValue(item, out var stack))
        {
            stack.Push(handle);
        }
        else
        {
            stack = new LinkedStack<IHeapHandle<TVal, TVal>>();
            stack.Push(handle);
            _map.Add(item, stack);
        }
    }

    public void Clear()
    {
        _map.Clear();
        _wrappedHeap.Clear();
    }

    public bool Contains(TVal item)
    {
        return _map.ContainsKey(item);
    }

    public void CopyTo(TVal[] array, int arrayIndex)
    {
        _wrappedHeap.CommonCopyTo(array, arrayIndex, h => h.Value);
    }

    public IEnumerator<TVal> GetEnumerator()
    {
        return _wrappedHeap.Select(p => p.Value).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Merge<TVal2>(IHeap<TVal2> other) where TVal2 : TVal
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        foreach (var h in other)
        {
            var handle = _wrappedHeap.Add(h, h);
            if (_map.TryGetValue(h, out var stack))
            {
                stack.Push(handle);
            }
            else
            {
                stack = new LinkedStack<IHeapHandle<TVal, TVal>>();
                stack.Push(handle);
                _map.Add(h, stack);
            }
        }
        other.Clear();
    }

    public bool Remove(TVal item)
    {
        if (_map.TryGetValue(item, out var stack))
        {
            var hasRemoved = _wrappedHeap.Remove(stack.Pop());
            Debug.Assert(hasRemoved);
            if (stack.Count == 0)
            {
                _map.Remove(item);
            }
            return true;
        }
        return false;
    }

    public TVal RemoveMin()
    {
        var item = _wrappedHeap.RemoveMin().Value;
        var stack = _map[item];
        stack.Pop();
        if (stack.Count == 0)
        {
            _map.Remove(item);
        }
        return item;
    }

    public IEnumerable<IReadOnlyTree<TVal>> ToReadOnlyForest()
    {
        foreach (var t in _wrappedHeap.ToReadOnlyForest())
        {
            var tt = t.BreadthFirstVisit<ReadOnlyTree<TVal>>((tree, parent) => TransformTree(tree, parent), null);
            yield return tt.ToList()[0];
        }
    }

    public override string ToString()
    {
        return _wrappedHeap.ToString();
    }

    private static ReadOnlyTree<TVal> TransformTree(IReadOnlyTree<TVal, TVal> tree, ReadOnlyTree<TVal> parent)
    {
        return new ReadOnlyTree<TVal>(tree.Priority, parent);
    }
}
