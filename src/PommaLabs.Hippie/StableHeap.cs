﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using PommaLabs.Hippie.Core;

namespace PommaLabs.Hippie;

public sealed class StableHeap<TVal, TPr> : IStableRawHeap<TVal, TPr>
{
    private readonly IRawHeap<TVal, IVersionedPriority<TPr>> _wrappedHeap;

    internal StableHeap(
        IRawHeap<TVal, IVersionedPriority<TPr>> wrappedHeap,
        IComparer<TPr> comparer,
        long initialVersion)
    {
        _wrappedHeap = wrappedHeap;
        Comparer = comparer;
        NextVersion = initialVersion;
    }

    public IComparer<TPr> Comparer { get; }

    public int Count
    {
        get { return _wrappedHeap.Count; }
    }

    public IEqualityComparer<TVal> EqualityComparer
    {
        get { return _wrappedHeap.EqualityComparer; }
    }

    public bool IsReadOnly
    {
        get { return _wrappedHeap.IsReadOnly; }
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> Min
    {
        get { return _wrappedHeap.Min; }
    }

    public long NextVersion { get; private set; }

    public TPr this[IHeapHandle<TVal, IVersionedPriority<TPr>> handle]
    {
        set { _wrappedHeap[handle] = new VersionedPriority<TPr>(value, NextVersion++); }
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> Add(TVal value, TPr priority)
    {
        return _wrappedHeap.Add(value, new VersionedPriority<TPr>(priority, NextVersion++));
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> Add(TVal value, TPr priority, long version)
    {
        return _wrappedHeap.Add(value, new VersionedPriority<TPr>(priority, version));
    }

    void IThinHeap<TVal, TPr>.Add(TVal value, TPr priority)
    {
        _wrappedHeap.Add(value, new VersionedPriority<TPr>(priority, NextVersion++));
    }

    void IStableThinHeap<TVal, TPr>.Add(TVal value, TPr priority, long version)
    {
        _wrappedHeap.Add(value, new VersionedPriority<TPr>(priority, version));
    }

    public void Add(IHeapHandle<TVal, TPr> item)
    {
        _wrappedHeap.Add(item.Value, new VersionedPriority<TPr>(item.Priority, NextVersion++));
    }

    public void Clear()
    {
        _wrappedHeap.Clear();
    }

    public bool Contains(IHeapHandle<TVal, IVersionedPriority<TPr>> handle)
    {
        return _wrappedHeap.Contains(handle);
    }

    public IEnumerator<IHeapHandle<TVal, IVersionedPriority<TPr>>> GetEnumerator()
    {
        return _wrappedHeap.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
        where TVal2 : TVal
        where TPr2 : TPr
    {
        this.CommonMerge(other);
    }

    public bool Remove(IHeapHandle<TVal, IVersionedPriority<TPr>> handle)
    {
        return _wrappedHeap.Remove(handle);
    }

    public bool Remove(IHeapHandle<TVal, TPr> handle)
    {
        return _wrappedHeap.Remove(UnwrapHandle(handle));
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> RemoveMin()
    {
        return _wrappedHeap.RemoveMin();
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        return _wrappedHeap.CommonToForest<TVal, IVersionedPriority<TPr>, TVal, TPr>((tree, parent) => TransformTree(tree, parent));
    }

    public override string ToString()
    {
        return _wrappedHeap.ToString();
    }

    public IVersionedPriority<TPr> UpdatePriorityOf(
        IHeapHandle<TVal, IVersionedPriority<TPr>> handle,
        TPr newPriority)
    {
        var newStablePriority = new VersionedPriority<TPr>(newPriority, NextVersion++);
        return _wrappedHeap.UpdatePriorityOf(handle, newStablePriority);
    }

    public IVersionedPriority<TPr> UpdatePriorityOf(
        IHeapHandle<TVal, IVersionedPriority<TPr>> handle,
        TPr newPriority, long newVersion)
    {
        var newStablePriority = new VersionedPriority<TPr>(newPriority, newVersion);
        return _wrappedHeap.UpdatePriorityOf(handle, newStablePriority);
    }

    public TVal UpdateValue(IHeapHandle<TVal, IVersionedPriority<TPr>> handle, TVal newValue)
    {
        return _wrappedHeap.UpdateValue(handle, newValue);
    }

    private static ReadOnlyTree<TVal, TPr> TransformTree(
        IReadOnlyTree<TVal, IVersionedPriority<TPr>> tree,
        ReadOnlyTree<TVal, TPr> parent)
    {
        return new ReadOnlyTree<TVal, TPr>(tree.Value, tree.Priority.Value, parent);
    }

    #region IRawHeap Members

    IHeapHandle<TVal, TPr> IThinHeap<TVal, TPr>.Min
    {
        get { return new StableHandle<TVal, TPr>(_wrappedHeap.Min); }
    }

    public TPr this[IHeapHandle<TVal, TPr> handle]
    {
        set { _wrappedHeap[UnwrapHandle(handle)] = new VersionedPriority<TPr>(value, NextVersion++); }
    }

    IHeapHandle<TVal, TPr> IRawHeap<TVal, TPr>.Add(TVal value, TPr priority)
    {
        var stablePriority = new VersionedPriority<TPr>(priority, NextVersion++);
        return new StableHandle<TVal, TPr>(_wrappedHeap.Add(value, stablePriority));
    }

    public bool Contains(IHeapHandle<TVal, TPr> handle)
    {
        return _wrappedHeap.Contains(UnwrapHandle(handle));
    }

    public void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        _wrappedHeap.CommonCopyTo(array, arrayIndex, h => new StableHandle<TVal, TPr>(h));
    }

    IEnumerator<IHeapHandle<TVal, TPr>> IEnumerable<IHeapHandle<TVal, TPr>>.GetEnumerator()
    {
        foreach (var handle in _wrappedHeap)
        {
            yield return new StableHandle<TVal, TPr>(handle);
        }
    }

    IHeapHandle<TVal, TPr> IThinHeap<TVal, TPr>.RemoveMin()
    {
        return new StableHandle<TVal, TPr>(_wrappedHeap.RemoveMin());
    }

    public TPr UpdatePriorityOf(IHeapHandle<TVal, TPr> handle, TPr newPriority)
    {
        var newStablePriority = new VersionedPriority<TPr>(newPriority, NextVersion++);
        return _wrappedHeap.UpdatePriorityOf(UnwrapHandle(handle), newStablePriority).Value;
    }

    public TVal UpdateValue(IHeapHandle<TVal, TPr> handle, TVal newValue)
    {
        return _wrappedHeap.UpdateValue(UnwrapHandle(handle), newValue);
    }

    private static IHeapHandle<TVal, IVersionedPriority<TPr>> UnwrapHandle(IHeapHandle<TVal, TPr> wrappedHandle)
    {
        return (wrappedHandle is StableHandle<TVal, TPr> stableHandle) ? stableHandle.Handle : throw new ArgumentException(ErrorMessages.InvalidHandle, nameof(wrappedHandle));
    }

    #endregion IRawHeap Members
}
