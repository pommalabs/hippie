﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core;

namespace PommaLabs.Hippie;

public sealed class BinomialHeap<TVal, TPr> : TreeHeap<TVal, TPr>
{
    private readonly Tree[] _addBuffer = new Tree[1];
    private readonly Tree[] _trees;
    private int _maxTreeIndex;

    internal BinomialHeap(IComparer<TPr> comparer)
        : base(comparer)
    {
        _trees = new Tree[(int)Math.Ceiling(Math.Log(int.MaxValue, 2))];
    }

    public override IHeapHandle<TVal, TPr> Add(TVal value, TPr priority)
    {
        // Preconditions
        if (priority is null) throw new ArgumentNullException(nameof(priority), ErrorMessages.NullPriority);

        var tree = new Tree(value, priority, Version);
        // We have to momentarily put the new tree in an enumerable array, in order to be able
        // to use the MergeTrees procedure.
        _addBuffer[0] = tree;
        MergeTrees(_addBuffer, 1, 1);
        // It's better to clear the buffer, in order to avoid to keep unnecessary references.
        _addBuffer[0] = null;
        // Only roots can became heap minimum.
        if (tree.Parent == null)
        {
            FixMin(tree);
        }
        var result = tree.Handle;

        // Postconditions
        Debug.Assert(result != null);
        Debug.Assert(Contains(result));
        Debug.Assert(EqualityComparer.Equals(result.Value, value));
        Debug.Assert(Comparer.Compare(result.Priority, priority) == 0);

        return result;
    }

    public override void Clear()
    {
        base.Clear();
        for (var i = 0; i < _trees.Length; ++i)
        {
            _trees[i] = null;
        }
        _maxTreeIndex = 0;
    }

    public override void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    public override IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        if (Count == 0)
        {
            yield break;
        }
        for (var i = 0; i <= _maxTreeIndex; ++i)
        {
            var tree = _trees[i];
            if (tree == null)
            {
                continue;
            }
            foreach (var t in tree.BreadthFirstVisit())
            {
                yield return t.Handle;
            }
        }
    }

    public override void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        if (other is not BinomialHeap<TVal, TPr> otherBinomialHeap)
        {
            this.CommonMerge(other);
            return;
        }
        otherBinomialHeap.Version.Id = Version.Id; // Updates all other nodes version
        MergeTrees(otherBinomialHeap._trees, otherBinomialHeap._trees.Length, otherBinomialHeap.Count);
        // Only roots can became heap minimum.
        if (otherBinomialHeap.MinTree.Parent == null)
        {
            FixMin(otherBinomialHeap.MinTree);
        }
        otherBinomialHeap.Clear();
    }

    public override bool Remove(IHeapHandle<TVal, TPr> item)
    {
        var treeHandle = GetHandle(item as TreeHandle);
        if (treeHandle == null)
        {
            return false;
        }
        var tree = treeHandle.Tree;
        for (var p = tree.Parent; p != null; tree = p, p = p.Parent)
        {
            tree.SwapRootWith(p);
        }
        foreach (var child in tree.Children)
        {
            child.Parent = null;
        }
        var treeOrder = tree.Children.Count;
        _trees[treeOrder] = null;
        MergeTrees(tree.Children, treeOrder, -1);
        if (ReferenceEquals(tree, MinTree))
        {
            FixMin();
        }
        tree.Handle.Version = null;
        return true;
    }

    public override IHeapHandle<TVal, TPr> RemoveMin()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

        var min = MinTree;
        foreach (var child in min.Children)
        {
            child.Parent = null;
        }
        var treeOrder = min.Children.Count;
        _trees[treeOrder] = null;
        MergeTrees(min.Children, treeOrder, -1);
        FixMin();
        min.Handle.Version = null;
        return min.Handle;
    }

    public override IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        if (Count == 0)
        {
            yield break;
        }
        for (var i = 0; i <= _maxTreeIndex; ++i)
        {
            var tree = _trees[i];
            if (tree != null)
            {
                yield return tree.ToReadOnlyTree();
            }
        }
    }

    public override string ToString()
    {
        return this.CommonToString();
    }

    protected override void MoveDown(TreeHandle handle)
    {
        var tree = handle.Tree;
        var initialTree = tree;
        var child = tree.MinChild(Compare);
        while (child != null && Compare(child.Priority, tree.Priority) < 0)
        {
            tree.SwapRootWith(child);
            tree = child;
            child = tree.MinChild(Compare);
        }
        if (ReferenceEquals(MinTree, initialTree))
        {
            FixMin();
        }
    }

    protected override void MoveUp(TreeHandle handle)
    {
        var tree = handle.Tree;
        var parent = tree.Parent;
        while (parent != null && Compare(tree.Priority, parent.Priority) < 0)
        {
            tree.SwapRootWith(parent);
            tree = parent;
            parent = parent.Parent;
        }
        if (parent == null)
        {
            FixMin(tree);
        }
    }

    private void FixMin()
    {
        MinTree = null;
        for (var i = 0; i <= _maxTreeIndex; ++i)
        {
            if (_trees[i] != null)
            {
                FixMin(_trees[i]);
            }
        }
    }

    private void FixMin(Tree tree)
    {
        Debug.Assert(tree.Parent == null);
        if (MinTree == null || Compare(tree.Priority, MinTree.Priority) <= 0)
        {
            MinTree = tree;
        }
    }

    /// <summary>
    /// </summary>
    /// <param name="otherTrees"></param>
    /// <param name="otherTreesCount"></param>
    /// <param name="otherPairCount"></param>
    /// <remarks>This is a performance critical method.</remarks>
    private void MergeTrees(IEnumerable<Tree> otherTrees, int otherTreesCount, int otherPairCount)
    {
        // If Count is zero, we have nothing to do. Moreover, we do not want to do Math.Log(0)...
        if ((Count += otherPairCount) == 0)
        {
            return;
        }
        _maxTreeIndex = (int)Math.Log(Count, 2);

        Tree carry = null;
        var en = otherTrees.GetEnumerator();
        for (var i = 0; i < otherTreesCount; ++i)
        {
            en.MoveNext();
            var merged = NullMeld(en.Current, _trees[i]);
            if (merged == null || merged.Children.Count == i + 1)
            {
                _trees[i] = carry;
                carry = merged;
            }
            else if (carry == null)
            {
                _trees[i] = merged;
            }
            else
            {
                _trees[i] = null;
                carry = Meld(merged, carry);
            }
        }

        if (carry == null)
        {
            return;
        }
        while (true)
        {
            var i = carry.Children.Count;
            carry = NullMeld(_trees[i], carry);
            if (carry.Children.Count == i)
            {
                _trees[i] = carry;
                break;
            }
            _trees[i] = null;
        }
    }
}
