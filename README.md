# Hippie

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docs][docfx-docs-badge]][docfx-docs]
[![NuGet version][nuget-version-badge]][nuget-package]
[![NuGet downloads][nuget-downloads-badge]][nuget-package]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Fully managed library providing five types of heap.

**Library is feature complete and no further development is planned on this project,
except for routine maintenance and bug fixes.**

Currently implemented heaps are:

- [Array][wikipedia-array-heap]
- [Binary][wikipedia-binary-heap]
- [Binomial][wikipedia-binomial-heap]
- [Fibonacci][wikipedia-fibonacci-heap]
- [Pairing][wikipedia-pairing-heap]

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Running tests](#running-tests)
- [License](#license)

## Install

NuGet package [PommaLabs.Hippie][nuget-package] is available for download:

```bash
dotnet add package PommaLabs.Hippie
```

## Usage

For example, using this library you can write a very simple heap sort in this way:

```cs
T[] HeapSort<T>(IEnumerable<T> elems) where T : IComparable<T>
{
    var heap = HeapFactory.NewBinaryHeap<T>();
    foreach (var elem in elems) {
        heap.Add(elem);
    }
    var orderedElems = new T[heap.Count];
    for (var i = 0; heap.Count > 0; ++i) {
        orderedElems[i] = heap.RemoveMin();
    }
    return orderedElems;
}
```

Please check [project examples][hippie-examples] to find out what you can do with this library.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `hippie.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

## License

MIT © 2012-2024 [PommaLabs Team and Contributors][pommalabs-website]

[docfx-docs]: https://hippie-docs.pommalabs.xyz/
[docfx-docs-badge]: https://img.shields.io/badge/DocFX-OK-green?style=flat-square
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/hippie/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/hippie/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[hippie-examples]: https://gitlab.com/pommalabs/hippie/-/tree/main/examples/PommaLabs.Hippie.Examples
[nuget-downloads-badge]: https://img.shields.io/nuget/dt/PommaLabs.Hippie?style=flat-square
[nuget-package]: https://www.nuget.org/packages/PommaLabs.Hippie/
[nuget-version-badge]: https://img.shields.io/nuget/v/PommaLabs.Hippie?style=flat-square
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/hippie/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_hippie?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_hippie?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_hippie
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
[wikipedia-array-heap]: https://en.wikipedia.org/wiki/D-ary_heap
[wikipedia-binary-heap]: https://en.wikipedia.org/wiki/Binary_heap
[wikipedia-binomial-heap]: https://en.wikipedia.org/wiki/Binomial_heap
[wikipedia-fibonacci-heap]: https://en.wikipedia.org/wiki/Fibonacci_heap
[wikipedia-pairing-heap]: https://en.wikipedia.org/wiki/Pairing_heap
