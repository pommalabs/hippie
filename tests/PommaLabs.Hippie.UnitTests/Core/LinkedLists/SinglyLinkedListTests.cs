﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal abstract class SinglyLinkedListTests<TList> : ThinLinkedListTests<TList> where TList : class, ILinkedList<int>
{
    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddLast_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddLast_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(0);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Append_DifferentItems(int itemCount)
    {
        var otherList = GetList();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
            otherList.Add(itemCount + i);
        }
        _list.Append(otherList);
        for (var i = 0; i < itemCount * 2; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(2 * itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void Append_EmptyList()
    {
        _list.Add(1);
        _list.Append(new SinglyLinkedList<int>());
        Assert.That(_list.Count, Is.EqualTo(1));
        Assert.That(_list.First, Is.EqualTo(1));
    }

    [Test]
    public void Append_NullList()
    {
        Should.Throw<ArgumentNullException>(() => _list.Append(null));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Append_SameItems(int itemCount)
    {
        var otherList = GetList();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
            otherList.Add(i);
        }
        _list.Append(otherList);
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(2 * itemCount - i));
            _list.RemoveFirst();
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void Last_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.Last);
    }

    [Test]
    public void Last_NoItems_AfterAddRemove()
    {
        _list.Add(5);
        _list.RemoveFirst();
        Should.Throw<InvalidOperationException>(() => _list.Last);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_DifferentItems_ReverseOrder(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = 0; i < itemCount - 1; ++i)
        {
            Assert.That(_list.Remove(i), Is.True);
            Assert.That(_list.Last, Is.EqualTo(i + 1));
        }
        Assert.That(_list.Remove(itemCount - 1), Is.True);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveFirst_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveFirst_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(0);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(0));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    protected override TList GetList()
    {
        var lst = (TList)(new SinglyLinkedList<int>() as ILinkedList<int>);
        ILinkedList<int> ret = new MockedLinkedList<TList, int>(lst);
        return (TList)ret;
    }

    protected override TList GetList(IEqualityComparer<int> equalityComparer)
    {
        var lst = (TList)(new SinglyLinkedList<int>(equalityComparer) as ILinkedList<int>);
        ILinkedList<int> ret = new MockedLinkedList<TList, int>(lst);
        return (TList)ret;
    }
}

[TestFixture]
internal sealed class SinglyLinkedListTests : SinglyLinkedListTests<ILinkedList<int>>
{
}
