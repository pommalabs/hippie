﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal abstract class ThinLinkedListTests<TList> where TList : class, IThinLinkedList<int>
{
    protected const int BigCount = 1000;
    protected const int MediumCount = 100;
    protected const int SmallCount = 10;
    protected TList _list;

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Add_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Add_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(0);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddFirst_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddFirst_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(0);
        }
        Assert.That(_list.Count, Is.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Contains_DifferentItems_NotContained(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = itemCount; i < itemCount * 2; ++i)
        {
            Assert.That(_list.Contains(i), Is.False);
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Contains_DifferentItems_RandomOrder(int itemCount)
    {
        var heap = new SortedList<int, int>();
        var rand = new Random();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
            int key;
            do { key = rand.Next(); } while (heap.ContainsKey(key));
            heap.Add(key, i);
        }
        foreach (var i in heap)
        {
            Assert.That(_list.Contains(i.Value), Is.True);
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Contains_DifferentItems_ReverseOrder(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.Contains(i), Is.True);
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Contains_DifferentItems_SameOrder(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = itemCount - 1; i >= 0; --i)
        {
            Assert.That(_list.Contains(i), Is.True);
        }
    }

    [Test]
    public void Factory_CustomEqualityComparer()
    {
        var eqCmp = new DummyEqualityComparer();
        Assert.That(eqCmp, Is.SameAs(GetList(eqCmp).EqualityComparer));
    }

    [Test]
    public void Factory_DefaultEqualityComparer()
    {
        Assert.That(EqualityComparer<int>.Default, Is.SameAs(GetList().EqualityComparer));
    }

    [Test]
    public void Factory_NullEqualityComparer()
    {
        Should.Throw<ArgumentNullException>(() => GetList(null));
    }

    [Test]
    public void First_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.First);
    }

    [Test]
    public void First_NoItems_AfterAddRemove()
    {
        _list.Add(5);
        _list.RemoveFirst();
        Should.Throw<InvalidOperationException>(() => _list.First);
    }

    [Test]
    public void Remove_ContainedItem()
    {
        _list.Add(0);
        Assert.That(_list.Remove(0), Is.True);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_DifferentItems_RandomOrder(int itemCount)
    {
        var heap = new SortedList<int, int>();
        var rand = new Random();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
            int key;
            do { key = rand.Next(); } while (heap.ContainsKey(key));
            heap.Add(key, i);
        }
        foreach (var i in heap)
        {
            Assert.That(_list.Remove(i.Value), Is.True);
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_DifferentItems_SameOrder(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = itemCount - 1; i > 0; --i)
        {
            Assert.That(_list.Remove(i), Is.True);
            Assert.That(_list.First, Is.EqualTo(i - 1));
        }
        Assert.That(_list.Remove(0), Is.True);
    }

    [Test]
    public void Remove_NotContainedItem()
    {
        Assert.That(_list.Remove(0), Is.False);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(0);
        }
        for (var i = itemCount - 1; i > 0; --i)
        {
            Assert.That(_list.Remove(0), Is.True);
            Assert.That(_list.First, Is.EqualTo(0));
        }
        Assert.That(_list.Remove(0), Is.True);
    }

    [Test]
    public void RemoveFirst_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.RemoveFirst());
    }

    [SetUp]
    public void SetUp()
    {
        _list = GetList();
    }

    [TearDown]
    public void TearDown()
    {
        _list = null;
    }

    protected virtual TList GetList()
    {
        var lst = (TList)(new ThinLinkedList<int>() as IThinLinkedList<int>);
        IThinLinkedList<int> ret = new MockedThinLinkedList<TList, int>(lst);
        return (TList)ret;
    }

    protected virtual TList GetList(IEqualityComparer<int> equalityComparer)
    {
        var lst = (TList)(new ThinLinkedList<int>(equalityComparer) as IThinLinkedList<int>);
        IThinLinkedList<int> ret = new MockedThinLinkedList<TList, int>(lst);
        return (TList)ret;
    }

    private sealed class DummyEqualityComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return true;
        }

        public int GetHashCode(int obj)
        {
            return 0;
        }
    }
}

[TestFixture]
internal sealed class ThinLinkedListTests : ThinLinkedListTests<IThinLinkedList<int>>
{
}
