﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal class MockedDoublyLinkedList<TList, TItem>(TList testList) : MockedLinkedList<TList, TItem>(testList), IDoublyLinkedList<TItem>
    where TList : class, IDoublyLinkedList<TItem>
{
    public void Append(IDoublyLinkedList<TItem> list)
    {
        if (list is not null)
        {
            foreach (var item in list)
            {
                RefList.AddLast(item);
            }
        }
        TestList.Append(list);
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
        Assert.That(TestList.Last, Is.EqualTo(RefList.Last.Value));
    }

    public IEnumerator<TItem> GetReversedEnumerator()
    {
        return TestList.GetReversedEnumerator();
    }

    public TItem RemoveLast()
    {
        var last = TestList.RemoveLast();
        RefList.RemoveLast();
        Assert.That(RefList, Has.Count.EqualTo(TestList.Count));
        if (TestList.Count > 0)
        {
            Assert.That(TestList.Last, Is.EqualTo(RefList.Last.Value));
        }
        return last;
    }

    public void Reverse()
    {
        TestList.Reverse();
        var testEn = TestList.GetReversedEnumerator();
        var refEn = RefList.GetEnumerator();
        while (testEn.MoveNext() && refEn.MoveNext())
        {
            Assert.That(refEn.Current, Is.EqualTo(testEn.Current));
        }
        Assert.That(refEn.MoveNext(), Is.EqualTo(testEn.MoveNext()));
        TestList.Reverse();
    }
}
