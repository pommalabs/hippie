﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Hippie.UnitTests;

/// <summary>
/// </summary>
public static class HeapSort
{
    public static T[] Sort<T>(IRawHeap<T, T> heap, IEnumerable<T> elems)
    {
        foreach (var elem in elems)
        {
            heap.Add(elem, elem);
        }
        var orderedElems = new T[heap.Count];
        for (var i = 0; heap.Count > 0; ++i)
        {
            orderedElems[i] = heap.RemoveMin().Value;
        }
        return orderedElems;
    }

    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="heap"></param>
    /// <param name="elems"></param>
    /// <returns></returns>
    public static T[] Sort<T>(IHeap<T, T> heap, IEnumerable<T> elems)
    {
        foreach (var elem in elems)
        {
            heap.Add(elem, elem);
        }
        var orderedElems = new T[heap.Count];
        for (var i = 0; heap.Count > 0; ++i)
        {
            orderedElems[i] = heap.RemoveMin().Value;
        }
        return orderedElems;
    }

    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="heap"></param>
    /// <param name="elems"></param>
    /// <returns></returns>
    public static T[] Sort<T>(IHeap<T> heap, IEnumerable<T> elems)
    {
        foreach (var elem in elems)
        {
            heap.Add(elem);
        }
        var orderedElems = new T[heap.Count];
        for (var i = 0; heap.Count > 0; ++i)
        {
            orderedElems[i] = heap.RemoveMin();
        }
        return orderedElems;
    }

    public static T[] Sort<T>(IThinHeap<T, T> heap, IEnumerable<T> elems) where T : struct
    {
        foreach (var elem in elems)
        {
            heap.Add(elem, elem);
        }
        var orderedElems = new T[heap.Count];
        for (var i = 0; heap.Count > 0; ++i)
        {
            orderedElems[i] = heap.RemoveMin().Value;
        }
        return orderedElems;
    }
}

public sealed class RandomGraph
{
    private const int MaxEdgeLength = 1000;
    private const int MinEdgeLength = 1;
    private readonly LinkedList<Edge>[] _edges;
    private readonly int _nodeCount;

    public RandomGraph(int nodeCount, double edgeProb)
    {
        _nodeCount = nodeCount;
        _edges = new LinkedList<Edge>[nodeCount];
        for (var i = 0; i < nodeCount; ++i)
        {
            _edges[i] = new LinkedList<Edge>();
        }

        var rand = new Random();
        for (var i = 0; i < nodeCount; ++i)
        {
            for (var j = 0; j < nodeCount; ++j)
            {
                if (i == j || rand.NextDouble() > edgeProb)
                {
                    continue;
                }
                var edgeLength = rand.Next(MinEdgeLength, MaxEdgeLength);
                _edges[i].AddLast(new Edge(j, edgeLength));
            }
        }
    }

    public void Clear()
    {
        foreach (var e in _edges)
        {
            e.Clear();
        }
    }

    public int[] Dijkstra(IRawHeap<int, int> prQueue, int start)
    {
        var distances = new int[_nodeCount];
        var visited = new bool[_nodeCount];
        var nodes = new IHeapHandle<int, int>[_nodeCount];
        for (var i = 0; i < _nodeCount; ++i)
        {
            nodes[i] = prQueue.Add(i, int.MaxValue);
            distances[i] = int.MaxValue;
        }
        prQueue[nodes[start]] = 0;

        while (prQueue.Count != 0)
        {
            var u = prQueue.RemoveMin();
            if (u.Priority == int.MaxValue)
            {
                break;
            }
            var uId = u.Value;
            distances[uId] = u.Priority;
            visited[uId] = true;
            foreach (var e in _edges[uId])
            {
                if (visited[e.Target])
                {
                    continue;
                }
                var tmpDist = u.Priority + e.Length;
                var v = nodes[e.Target];
                if (tmpDist < v.Priority)
                {
                    prQueue[v] = tmpDist;
                }
            }
        }

        return distances;
    }

    public int[] Dijkstra(IHeap<int, int> prQueue, int start)
    {
        var distances = new int[_nodeCount];
        var visited = new bool[_nodeCount];
        for (var i = 0; i < _nodeCount; ++i)
        {
            distances[i] = int.MaxValue;
            prQueue.Add(i, int.MaxValue);
        }
        prQueue.UpdatePriorityOf(start, 0);

        while (prQueue.Count != 0)
        {
            var u = prQueue.RemoveMin();
            if (u.Priority == int.MaxValue)
            {
                break;
            }
            var uId = u.Value;
            distances[uId] = u.Priority;
            visited[uId] = true;
            foreach (var e in _edges[uId])
            {
                if (visited[e.Target])
                {
                    continue;
                }
                var tmpDist = u.Priority + e.Length;
                var v = e.Target;
                if (tmpDist < prQueue[v])
                {
                    prQueue.UpdatePriorityOf(v, tmpDist);
                }
            }
        }

        return distances;
    }

    private struct Edge
    {
        public readonly int Length;
        public readonly int Target;

        public Edge(int target, int length)
        {
            Target = target;
            Length = length;
        }
    }
}
