﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;
using NUnit.Framework;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests;

public sealed class MultiArrayHeapTests : MultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return HeapFactory.NewArrayHeap<T>(7);
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return HeapFactory.NewArrayHeap(7, eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewArrayHeap(7, cmp);
    }
}

public sealed class MultiBinaryHeapTests : MultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return HeapFactory.NewBinaryHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return HeapFactory.NewBinaryHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewBinaryHeap(cmp);
    }
}

public sealed class MultiBinomialHeapTests : MultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return HeapFactory.NewBinomialHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return HeapFactory.NewBinomialHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewBinomialHeap(cmp);
    }
}

public sealed class MultiFibonacciHeapTests : MultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return HeapFactory.NewFibonacciHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return HeapFactory.NewFibonacciHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewFibonacciHeap(cmp);
    }
}

public abstract class MultiHeapTests : HeapTestsBase
{
    protected const int ManyIntItemsCount = 6120;
    private const int FewIntItemsCount = ManyIntItemsCount / 100;
    private const int RandomTestsRepetitionCount = 5;

    private readonly MultiHeap<int> _refIntHeap = HeapFactory.NewBinaryHeap<int>();
    private IHeap<int> _intHeap;

    [Test]
    public void Add_FakeIntEqualityComparer()
    {
        _intHeap = GetHeap(new FakeIntEqualityComparer());
        _intHeap.Add(1);
        _intHeap.Add(2);
        _intHeap.Add(3);
        Assert.That(_intHeap, Has.Count.EqualTo(3));
    }

    [Test]
    public void Add_FewOrderedIntItems()
    {
        Add_MainTest(AddOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Add_FewRandomIntItems()
    {
        Add_MainTest(AddRandomIntItems, FewIntItemsCount);
    }

    [Test]
    public void Add_FewReverseOrderedIntItems()
    {
        Add_MainTest(AddReverseOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    public void Add_FewSameIntItems()
    {
        Add_MainTest(AddSameIntItems, FewIntItemsCount);
    }

    [Test]
    public void Add_ManyOrderedIntItems()
    {
        Add_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Add_ManyRandomIntItems()
    {
        Add_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Add_ManyReverseOrderedIntItems()
    {
        Add_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Add_ManySameIntItems()
    {
        Add_MainTest(AddSameIntItems);
    }

    [Test]
    public void Add_ReversedIntComparer()
    {
        _intHeap = GetHeap(new ReversedIntComparer());
        _intHeap.Add(1);
        _intHeap.Add(2);
        _intHeap.Add(3);
        Assert.That(_intHeap.RemoveMin(), Is.EqualTo(3));
        Assert.That(_intHeap.RemoveMin(), Is.EqualTo(2));
        Assert.That(_intHeap.RemoveMin(), Is.EqualTo(1));
    }

    [Test]
    public void Clear_FewOrderedIntItems()
    {
        Clear_MainTest(AddOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Clear_FewRandomIntItems()
    {
        Clear_MainTest(AddRandomIntItems, FewIntItemsCount);
    }

    [Test]
    public void Clear_FewReverseOrderedIntItems()
    {
        Clear_MainTest(AddReverseOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    public void Clear_FewSameIntItems()
    {
        Clear_MainTest(AddSameIntItems, FewIntItemsCount);
    }

    [Test]
    public void Clear_ManyOrderedIntItems()
    {
        Clear_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Clear_ManyRandomIntItems()
    {
        Clear_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Clear_ManyReverseOrderedIntItems()
    {
        Clear_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Clear_ManySameIntItems()
    {
        Clear_MainTest(AddSameIntItems);
    }

    [Test]
    public void Contains_FakeIntEqualityComparer()
    {
        _intHeap = GetHeap(new FakeIntEqualityComparer());
        _intHeap.Add(1);
        Assert.That(_intHeap.Contains(1), Is.True);
        Assert.That(_intHeap.Contains(10), Is.True);
        Assert.That(_intHeap.Contains(100), Is.True);
        _intHeap.Add(5);
        Assert.That(_intHeap.Contains(1), Is.True);
        Assert.That(_intHeap.Contains(10), Is.True);
        Assert.That(_intHeap.Contains(100), Is.True);
        Assert.That(_intHeap.Contains(5), Is.True);
        Assert.That(_intHeap.Contains(50), Is.True);
        Assert.That(_intHeap.Contains(500), Is.True);
    }

    [Test]
    public void Contains_FewOrderedIntItems()
    {
        Contains_MainTest(AddOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Contains_FewRandomIntItems()
    {
        Contains_MainTest(AddRandomIntItems, FewIntItemsCount);
    }

    [Test]
    public void Contains_FewReverseOrderedIntItems()
    {
        Contains_MainTest(AddReverseOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    public void Contains_FewSameIntItems()
    {
        Contains_MainTest(AddSameIntItems, FewIntItemsCount);
    }

    [Test]
    public void Contains_ManyOrderedIntItems()
    {
        Contains_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Contains_ManyRandomIntItems()
    {
        Contains_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Contains_ManyReverseOrderedIntItems()
    {
        Contains_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Contains_ManySameIntItems()
    {
        Contains_MainTest(AddSameIntItems);
    }

    [Test]
    public void HeapSort_FewOrderedIntItems()
    {
        HeapSort_MainTest(AddOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void HeapSort_FewRandomIntItems()
    {
        HeapSort_MainTest(AddRandomIntItems, FewIntItemsCount);
    }

    [Test]
    public void HeapSort_FewReverseOrderedIntItems()
    {
        HeapSort_MainTest(AddReverseOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    public void HeapSort_FewSameIntItems()
    {
        HeapSort_MainTest(AddSameIntItems, FewIntItemsCount);
    }

    [Test]
    public void HeapSort_ManyOrderedIntItems()
    {
        HeapSort_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void HeapSort_ManyRandomIntItems()
    {
        HeapSort_MainTest(AddRandomIntItems);
    }

    [Test]
    public void HeapSort_ManyReverseOrderedIntItems()
    {
        HeapSort_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void HeapSort_ManySameIntItems()
    {
        HeapSort_MainTest(AddSameIntItems);
    }

    [Test]
    public void Merge_Covariant()
    {
        var h1 = HeapFactory.NewBinaryHeap<A>();
        var h2 = HeapFactory.NewBinaryHeap<B>();

        h1.Add(new A(0));
        h2.Add(new B(1));
        h1.Merge(h2);

        Assert.That(h2, Is.Empty);
        Assert.That(h1, Has.Count.EqualTo(2));
    }

    [Test]
    public void Merge_DifferentHeapType_ManyOrderedIntItems()
    {
        Merge_DifferentHeapType_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Merge_DifferentHeapType_ManyRandomIntItems()
    {
        Merge_DifferentHeapType_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Merge_DifferentHeapType_ManyReverseOrderedIntItems()
    {
        Merge_DifferentHeapType_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Merge_DifferentHeapType_ManySameIntItems()
    {
        Merge_DifferentHeapType_MainTest(AddSameIntItems);
    }

    [Test]
    public void Merge_EmptyHeap_WithFullHeap()
    {
        var otherIntHeap = HeapFactory.NewArrayHeap<int>(3);
        AddRandomIntItems(ManyIntItemsCount, otherIntHeap);
        _intHeap.Merge(otherIntHeap);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_NullOtherHeap()
    {
        Should.Throw<ArgumentNullException>(() => _intHeap.Merge<int>(null));
    }

    [Test]
    public void Merge_SameHeap()
    {
        AddRandomIntItems();
        _intHeap.Merge(_intHeap);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_SameHeapType_ManyOrderedIntItems()
    {
        Merge_SameHeapType_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Merge_SameHeapType_ManyRandomIntItems()
    {
        Merge_SameHeapType_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Merge_SameHeapType_ManyReverseOrderedIntItems()
    {
        Merge_SameHeapType_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Merge_SameHeapType_ManySameIntItems()
    {
        Merge_SameHeapType_MainTest(AddSameIntItems);
    }

    [Test]
    public void Merge_WithEmptyHeap()
    {
        AddRandomIntItems();
        var otherIntHeap = HeapFactory.NewArrayHeap<int>(3);
        _intHeap.Merge(otherIntHeap);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_WithFullHeap_WithDifferentComparer()
    {
        AddRandomIntItems(ManyIntItemsCount, _intHeap);
        var heap = HeapFactory.NewArrayHeap(21, new ReversedIntComparer());
        AddRandomIntItems(ManyIntItemsCount, heap);
        Should.Throw<ArgumentException>(() => _intHeap.Merge(heap));
    }

    [Test]
    public void Merge_WithFullHeap_WithSameValues()
    {
        AddRandomIntItems(ManyIntItemsCount, _intHeap); // Method also adds values to RefIntHeap
        _intHeap.Merge(_refIntHeap);
        Assert.That(_intHeap, Has.Count.EqualTo(2 * ManyIntItemsCount));
    }

    [Test]
    public void Remove_AddRemove_NotExistingItem()
    {
        _intHeap.Add(0);
        Assert.That(_intHeap.Remove(0), Is.True);
        Assert.That(_intHeap.Remove(0), Is.False);
    }

    [Test]
    public void Remove_FakeIntEqualityComparer()
    {
        _intHeap = GetHeap(new FakeIntEqualityComparer());
        _intHeap.Add(1);
        _intHeap.Remove(10);
        Assert.That(_intHeap, Is.Empty);
        _intHeap.Add(5);
        _intHeap.Remove(5);
        Assert.That(_intHeap, Is.Empty);
    }

    [Test]
    public void Remove_FewOrderedIntItems()
    {
        Remove_MainTest(AddOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Remove_FewRandomIntItems()
    {
        Remove_MainTest(AddRandomIntItems, FewIntItemsCount);
    }

    [Test]
    public void Remove_FewReverseOrderedIntItems()
    {
        Remove_MainTest(AddReverseOrderedIntItems, FewIntItemsCount);
    }

    [Test]
    public void Remove_FewSameIntItems()
    {
        Remove_MainTest(AddSameIntItems, FewIntItemsCount);
    }

    [Test]
    public void Remove_ManyOrderedIntItems()
    {
        Remove_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void Remove_ManyRandomIntItems()
    {
        Remove_MainTest(AddRandomIntItems);
    }

    [Test]
    public void Remove_ManyReverseOrderedIntItems()
    {
        Remove_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void Remove_ManySameIntItems()
    {
        Remove_MainTest(AddSameIntItems);
    }

    [Test]
    public void Remove_NotExistingItem()
    {
        Assert.That(_intHeap.Remove(0), Is.False);
    }

    [SetUp]
    public void SetUp()
    {
        _intHeap = GetHeap<int>();
    }

    [TearDown]
    public void TearDown()
    {
        _refIntHeap.Clear();
        _intHeap = null;
    }

    [Test]
    public void ToReadOnlyForest_Generic_ManyOrderedIntItems()
    {
        ToReadOnlyForest_Generic_MainTest(AddOrderedIntItems);
    }

    [Test]
    [Repeat(RandomTestsRepetitionCount)]
    public void ToReadOnlyForest_Generic_ManyRandomIntItems()
    {
        ToReadOnlyForest_Generic_MainTest(AddRandomIntItems);
    }

    [Test]
    public void ToReadOnlyForest_Generic_ManyReverseOrderedIntItems()
    {
        ToReadOnlyForest_Generic_MainTest(AddReverseOrderedIntItems);
    }

    [Test]
    public void ToReadOnlyForest_Generic_ManySameIntItems()
    {
        ToReadOnlyForest_Generic_MainTest(AddSameIntItems);
    }

    [Test]
    public void ToReadOnlyForest_OneElement()
    {
        _intHeap.Add(0);
        var f = _intHeap.ToReadOnlyForest().ToList();
        Assert.That(f, Has.Count.EqualTo(1));
        var t = f[0];
        Assert.That(t, Is.Not.Null);
        Assert.That(t.Parent, Is.Null);
        Assert.That(t.Item, Is.EqualTo(0));
        Assert.That(t.Children, Is.Not.Null);
        Assert.That(t.Children.Count(), Is.EqualTo(0));
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_EmptyHeap()
    {
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_FullHeap()
    {
        AddRandomIntItems();
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    protected abstract IHeap<T> GetHeap<T>() where T : IComparable<T>;

    protected abstract IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp) where T : IComparable<T>;

    protected abstract IHeap<T> GetHeap<T>(IComparer<T> cmp);

    private static void AssertSameContents<T>(MultiHeap<T> refHeap, IHeap<T> heap)
        where T : IComparable<T>
    {
        Assert.That(heap, Has.Count.EqualTo(refHeap.Count));

        foreach (var p in refHeap)
        {
            Assert.That(heap.Contains(p), Is.True);
        }

        foreach (var p in heap)
        {
            Assert.That(refHeap.Contains(p), Is.True);
        }

        while (refHeap.Count != 0)
        {
            Assert.That(heap.Min, Is.EqualTo(refHeap.Min));
            Assert.That(heap.RemoveMin(), Is.EqualTo(refHeap.RemoveMin()));
            Assert.That(heap, Has.Count.EqualTo(refHeap.Count));
        }

        Assert.That(heap, Is.Empty);
    }

    private void Add_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    private void AddOrderedIntItems(int count = ManyIntItemsCount, IHeap<int> heap = null)
    {
        Debug.Assert(count <= ManyIntItemsCount);
        heap ??= _intHeap;
        for (var i = 0; i < count / 2; ++i)
        {
            // Values are added twice, because we want duplicates
            _refIntHeap.Add(i);
            _refIntHeap.Add(i);
            heap.Add(i);
            heap.Add(i);
        }
    }

    private void AddRandomIntItems(int count = ManyIntItemsCount, IHeap<int> heap = null)
    {
        Debug.Assert(count <= ManyIntItemsCount);
        heap ??= _intHeap;
        for (var i = 0; i < count; ++i)
        {
            var r = Rand.Next(0, 50); // We want duplicates, if possible
            _refIntHeap.Add(r);
            heap.Add(r);
        }
    }

    private void AddReverseOrderedIntItems(int count = ManyIntItemsCount, IHeap<int> heap = null)
    {
        Debug.Assert(count <= ManyIntItemsCount);
        heap ??= _intHeap;
        for (var i = count / 2; i > 0; --i)
        {
            // Values are added twice, because we want duplicates
            _refIntHeap.Add(i);
            _refIntHeap.Add(i);
            heap.Add(i);
            heap.Add(i);
        }
    }

    private void AddSameIntItems(int count = ManyIntItemsCount, IHeap<int> heap = null)
    {
        Debug.Assert(count <= ManyIntItemsCount);
        heap ??= _intHeap;
        for (var i = 0; i < count; ++i)
        {
            _refIntHeap.Add(0);
            heap.Add(0);
        }
    }

    private void Clear_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        _intHeap.Clear();
        foreach (var item in _refIntHeap)
            Assert.That(_intHeap.Contains(item), Is.False);
        Assert.That(_intHeap, Is.Empty);
    }

    private void Contains_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        foreach (var item in _refIntHeap)
            Assert.That(_intHeap.Contains(item), Is.True);
    }

    private void HeapSort_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        var list = _refIntHeap.ToList();
        list.Sort();
        foreach (var item in list)
            Assert.That(_intHeap.RemoveMin(), Is.EqualTo(item));
        Assert.That(_intHeap, Is.Empty);
    }

    private void Merge_DifferentHeapType_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        var otherIntHeap = HeapFactory.NewArrayHeap<int>(30);
        addMethod(count, otherIntHeap);
        _intHeap.Merge(otherIntHeap);
        Assert.That(otherIntHeap, Is.Empty);

        addMethod(count, null);
        addMethod(count, otherIntHeap);
        _intHeap.Merge(otherIntHeap);
        Assert.That(otherIntHeap, Is.Empty);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    private void Merge_SameHeapType_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        var otherIntHeap = GetHeap<int>();
        addMethod(count, otherIntHeap);
        _intHeap.Merge(otherIntHeap);
        Assert.That(otherIntHeap, Is.Empty);

        addMethod(count, null);
        addMethod(count, otherIntHeap);
        _intHeap.Merge(otherIntHeap);
        Assert.That(otherIntHeap, Is.Empty);
        AssertSameContents(_refIntHeap, _intHeap);
    }

    private void Remove_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        foreach (var item in _refIntHeap)
            Assert.That(_intHeap.Remove(item), Is.True);
        Assert.That(_intHeap, Is.Empty);
    }

    private void ToReadOnlyForest_Generic_MainTest(Action<int, IHeap<int>> addMethod, int count = ManyIntItemsCount)
    {
        addMethod(count, null);
        var items = new HashSet<int>(_intHeap);
        var forest = _intHeap.ToReadOnlyForest().ToList();
        // Breadth-first, no action
        var trees = forest.SelectMany(t => t.BreadthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(count));
        foreach (var t in trees)
        {
            Assert.That(items.Contains(t.Item));
        }
        // Depth-first, no action
        trees = forest.SelectMany(t => t.DepthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(count));
        foreach (var t in trees)
        {
            Assert.That(items.Contains(t.Item));
        }
        // Breadth-first, with selector
        var res = forest.SelectMany(t => t.BreadthFirstVisit((n, a) => n.Item, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(count));
        foreach (var r in res)
        {
            Assert.That(items.Contains(r));
        }
        // Depth-first, with selector
        res = forest.SelectMany(t => t.DepthFirstVisit((n, a) => n.Item, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(count));
        foreach (var r in res)
        {
            Assert.That(items.Contains(r));
        }
        // Breadth-first, with action
        var set = new HashSet<int>();
        forest.ForEach(t => t.BreadthFirstVisit(n => set.Add(n.Item)));
        Assert.That(set, Has.Count.EqualTo(items.Count));
        foreach (var s in set)
        {
            Assert.That(items.Contains(s));
        }
        // Depth-first, with action
        set = [];
        forest.ForEach(t => t.DepthFirstVisit(n => set.Add(n.Item)));
        Assert.That(set, Has.Count.EqualTo(items.Count));
        foreach (var s in set)
        {
            Assert.That(items.Contains(s));
        }
    }
}

public sealed class MultiPairingHeapTests : MultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return HeapFactory.NewPairingHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return HeapFactory.NewPairingHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewPairingHeap(cmp);
    }
}

public sealed class StableMultiArrayHeapTests : StableMultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return StableHeapFactory.NewArrayHeap<T>(7);
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return StableHeapFactory.NewArrayHeap(7, eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewArrayHeap(7, cmp);
    }
}

public sealed class StableMultiBinaryHeapTests : StableMultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return StableHeapFactory.NewBinaryHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return StableHeapFactory.NewBinaryHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewBinaryHeap(cmp);
    }
}

public sealed class StableMultiBinomialHeapTests : StableMultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return StableHeapFactory.NewBinomialHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return StableHeapFactory.NewBinomialHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewBinomialHeap(cmp);
    }
}

public sealed class StableMultiFibonacciHeapTests : StableMultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return StableHeapFactory.NewFibonacciHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return StableHeapFactory.NewFibonacciHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewFibonacciHeap(cmp);
    }
}

public abstract class StableMultiHeapTests : MultiHeapTests
{
    [TestCase(1)]
    [TestCase(ManyIntItemsCount / 2)]
    [TestCase(ManyIntItemsCount)]
    public void Add_SamePriority(int valueCount)
    {
        var heap = GetHeap<TheStrangeDuo>();
        for (var i = 0; i < valueCount; ++i)
        {
            heap.Add(new TheStrangeDuo(i, 0));
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(heap.Min.Aa, Is.EqualTo(i));
            Assert.That(heap.Min.Bb, Is.EqualTo(0));
            var min = heap.RemoveMin();
            Assert.That(min.Aa, Is.EqualTo(i));
            Assert.That(min.Bb, Is.EqualTo(0));
        }
    }

    private readonly struct TheStrangeDuo(int a, int b) : IComparable<TheStrangeDuo>
    {
        public readonly int Aa = a;
        public readonly int Bb = b;

        public int CompareTo(TheStrangeDuo other)
        {
            return Bb.CompareTo(other.Bb);
        }
    }
}

public sealed class StableMultiPairingHeapTests : StableMultiHeapTests
{
    protected override IHeap<T> GetHeap<T>()
    {
        return StableHeapFactory.NewPairingHeap<T>();
    }

    protected override IHeap<T> GetHeap<T>(IEqualityComparer<T> eqCmp)
    {
        return StableHeapFactory.NewPairingHeap(eqCmp);
    }

    protected override IHeap<T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewPairingHeap(cmp);
    }
}
