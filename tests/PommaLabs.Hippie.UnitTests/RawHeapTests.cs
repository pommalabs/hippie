﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests;

public sealed class RawArrayHeapTests : RawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawArrayHeap<T, T>(7);
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawArrayHeap<T, T>(7, cmp);
    }
}

public sealed class RawBinaryHeapTests : RawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawBinaryHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawBinaryHeap<T, T>(cmp);
    }
}

public sealed class RawBinomialHeapTests : RawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawBinomialHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawBinomialHeap<T, T>(cmp);
    }
}

public sealed class RawFibonacciHeapTests : RawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawFibonacciHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawFibonacciHeap<T, T>(cmp);
    }
}

public abstract class RawHeapTests : ValPrHeapTests
{
    protected IRawHeap<int, int> _intHeap;

    private static readonly Dictionary<int, IHeapHandle<int, int>> s_intHandles;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_randIntHandles;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_refIntHandles;
    private static readonly ArrayHeap<int, int> s_refIntHeap;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_refRandIntHandles;
    private static readonly Dictionary<int, IHeapHandle<string, string>> s_refStringHandles;
    private static readonly ArrayHeap<string, string> s_refStringHeap;
    private static readonly Dictionary<int, IHeapHandle<string, string>> s_stringHandles;

    private IRawHeap<string, string> _stringHeap;

    static RawHeapTests()
    {
        s_intHandles = [];
        s_refIntHandles = [];
        s_stringHandles = [];
        s_refStringHandles = [];
        s_randIntHandles = [];
        s_refRandIntHandles = [];
        s_refIntHeap = HeapFactory.NewRawBinaryHeap<int, int>();
        s_refStringHeap = HeapFactory.NewRawBinaryHeap<string, string>();
    }

    protected RawHeapTests()
    {
        RandSet.Clear();
    }

    [Test]
    public void Add_ManyIntItems()
    {
        AddIntValues(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_ManyRandIntItems()
    {
        AddRandomIntValues(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_ManyStringItems()
    {
        AddStringValues(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void Add_NullArguments()
    {
        Should.Throw<ArgumentNullException>(() => _stringHeap.Add(null, null));
    }

    [Test]
    public void Add_NullStringPriority()
    {
        Should.Throw<ArgumentNullException>(() => _stringHeap.Add(StringValues[0], null));
    }

    [Test]
    public void Add_OneIntItem()
    {
        AddIntValues(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_OneRandIntItem()
    {
        AddRandomIntValues(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_OneStringItem()
    {
        AddStringValues(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void Add_ReversedIntComparer()
    {
        _intHeap = GetHeap(new ReversedIntComparer());
        _intHeap.Add(1, 10);
        _intHeap.Add(2, 20);
        _intHeap.Add(3, 30);
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(30));
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(20));
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(10));
    }

    [Test]
    public void Add_SameIntValues()
    {
        Assert.DoesNotThrow(() => AddSameIntValues(_intHeap));
    }

    [Test]
    public void Add_SameStringValues()
    {
        AddStringValues(_stringHeap);
        Should.Throw<ArgumentException>(() => AddStringValues(_stringHeap));
    }

    [Test]
    public void Clear_EmptyHeap()
    {
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
    }

    [Test]
    public void Clear_FullHeap()
    {
        AddIntValues(_intHeap);
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
        foreach (var p in s_intHandles)
            Assert.That(_intHeap.Contains(p.Value), Is.False);
    }

    [Test]
    public void Clear_TwoTimes()
    {
        AddIntValues(_intHeap);
        _intHeap.Clear();
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
        foreach (var p in s_intHandles)
            Assert.That(_intHeap.Contains(p.Value), Is.False);
    }

    [Test]
    public void Dijkstra_EmptyGraph()
    {
        Dijkstra(EmptyGraph);
    }

    [Test]
    public void Dijkstra_MediumGraph()
    {
        Dijkstra(MediumGraph);
    }

    [Test]
    public void Dijkstra_SparseGraph()
    {
        Dijkstra(SparseGraph);
    }

    [Test]
    public void HeapSort_IntValues()
    {
        HeapSort_Test(s_refIntHeap, _intHeap, IntValues);
    }

    [Test]
    public void HeapSort_RandomIntValues()
    {
        HeapSort_Test(s_refIntHeap, _intHeap, RandomIntValues);
    }

    [Test]
    public void HeapSort_StringValues()
    {
        HeapSort_Test(s_refStringHeap, _stringHeap, StringValues);
    }

    [Test]
    public void Merge_CovariantHeaps()
    {
        var aHeap = GetHeap<A>();
        var bHeap = GetHeap<B>();
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, aItem);
            var bItem = new B(rand);
            bHeap.Add(bItem, bItem);
        }
        aHeap.Merge(bHeap);
        Assert.That(aHeap, Has.Count.EqualTo(200));
        Assert.That(bHeap, Is.Empty);
    }

    [Test]
    public void Merge_CovariantHeaps_TwoTimes()
    {
        var aHeap = GetHeap<A>();
        var bHeap = GetHeap<B>();
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, aItem);
            var bItem = new B(rand);
            bHeap.Add(bItem, bItem);
        }
        aHeap.Merge(bHeap);
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, aItem);
            var bItem = new B(rand);
            bHeap.Add(bItem, bItem);
        }
        aHeap.Merge(bHeap);
        Assert.That(aHeap, Has.Count.EqualTo(400));
        Assert.That(bHeap, Is.Empty);
    }

    [Test]
    public void Merge_EmptyHeap_WithEmptyHeap_OfDifferentType()
    {
        var heap = HeapFactory.NewRawArrayHeap<int, int>(21);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithEmptyHeap_OfSameType()
    {
        var heap = GetHeap<int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithFullHeap_OfDifferentType()
    {
        var heap = HeapFactory.NewRawArrayHeap<int, int>(21);
        AddIntValues(heap);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithFullHeap_OfSameType()
    {
        var heap = GetHeap<int>();
        AddIntValues(heap);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithNullHeap()
    {
        Should.Throw<ArgumentNullException>(() => _intHeap.Merge<int, int>(null));
    }

    [Test]
    public void Merge_FullHeap_WithEmptyHeap_OfDifferentType()
    {
        AddIntValues(_intHeap);
        var heap = HeapFactory.NewRawArrayHeap<int, int>(21);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithEmptyHeap_OfSameType()
    {
        AddIntValues(_intHeap);
        var heap = GetHeap<int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithFullHeap_OfDifferentType()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = HeapFactory.NewRawArrayHeap<int, int>(21);
        AddIntValues(heap, IntValueCount / 2);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithFullHeap_OfSameType()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = GetHeap<int>();
        AddIntValues(heap, IntValueCount / 2);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithNullHeap()
    {
        AddIntValues(_intHeap);
        Should.Throw<ArgumentNullException>(() => _intHeap.Merge<int, int>(null));
    }

    [Test]
    public void Merge_WithFullHeap_WithDifferentComparer()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = HeapFactory.NewRawArrayHeap<int, int>(21, new ReversedIntComparer());
        AddIntValues(heap, IntValueCount / 2);
        Should.Throw<ArgumentException>(() => _intHeap.Merge(heap));
    }

    [Test]
    public void Merge_WithFullHeap_WithSameValues()
    {
        AddIntValues(_intHeap); // Method also adds values to RefIntHeap
        _intHeap.Merge(s_refIntHeap);
        Assert.That(_intHeap, Has.Count.EqualTo(2 * IntValueCount));
    }

    [Test]
    public void Merge_WithSameHeap()
    {
        AddIntValues(_intHeap);
        _intHeap.Merge(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Min_EmptyHeap()
    {
        Should.Throw<InvalidOperationException>(() => _intHeap.Min);
    }

    [Test]
    public void RemoveMin_EmptyHeap()
    {
        Should.Throw<InvalidOperationException>(() => _intHeap.RemoveMin());
    }

    [SetUp]
    public void SetUp()
    {
        _intHeap = GetHeap<int>();
        _stringHeap = GetHeap<string>();
    }

    [TearDown]
    public void TearDown()
    {
        s_intHandles.Clear();
        s_refIntHandles.Clear();
        s_stringHandles.Clear();
        s_refStringHandles.Clear();
        s_randIntHandles.Clear();
        s_refRandIntHandles.Clear();
        s_refIntHeap.Clear();
        s_refStringHeap.Clear();
        _intHeap = null;
        _stringHeap = null;
    }

    [Test]
    [Repeat(3)]
    public void ToReadOnlyForest_Generic()
    {
        AddRandomIntValues(_intHeap);
        var values = new HashSet<int>(_intHeap.Select(p => p.Value));
        var priorities = new HashSet<int>(_intHeap.Select(p => p.Priority));
        var forest = _intHeap.ToReadOnlyForest().ToList();
        // Breadth-first, no action
        var trees = forest.SelectMany(t => t.BreadthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(IntValueCount));
        foreach (var t in trees)
        {
            Assert.That(values.Contains(t.Value));
            Assert.That(priorities.Contains(t.Priority));
        }
        // Depth-first, no action
        trees = forest.SelectMany(t => t.DepthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(IntValueCount));
        foreach (var t in trees)
        {
            Assert.That(values.Contains(t.Value));
            Assert.That(priorities.Contains(t.Priority));
        }
        // Breadth-first, with selector
        var res = forest.SelectMany(t => t.BreadthFirstVisit((n, a) => n.Priority, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(IntValueCount));
        foreach (var r in res)
        {
            Assert.That(priorities.Contains(r));
        }
        // Depth-first, with selector
        res = forest.SelectMany(t => t.DepthFirstVisit((n, a) => n.Priority, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(IntValueCount));
        foreach (var r in res)
        {
            Assert.That(priorities.Contains(r));
        }
        // Breadth-first, with action
        var set = new HashSet<int>();
        forest.ForEach(t => t.BreadthFirstVisit(n => set.Add(n.Priority)));
        Assert.That(set, Has.Count.EqualTo(priorities.Count));
        foreach (var s in set)
        {
            Assert.That(priorities.Contains(s));
        }
        // Depth-first, with action
        set = [];
        forest.ForEach(t => t.DepthFirstVisit(n => set.Add(n.Priority)));
        Assert.That(set, Has.Count.EqualTo(priorities.Count));
        foreach (var s in set)
        {
            Assert.That(priorities.Contains(s));
        }
    }

    [Test]
    public void ToReadOnlyForest_OneElement()
    {
        _intHeap.Add(IntValues[0], 0);
        var f = _intHeap.ToReadOnlyForest().ToList();
        Assert.That(f, Has.Count.EqualTo(1));
        var t = f[0];
        Assert.That(t, Is.Not.Null);
        Assert.That(t.Parent, Is.Null);
        Assert.That(t.Value, Is.EqualTo(IntValues[0]));
        Assert.That(t.Priority, Is.EqualTo(0));
        Assert.That(t.Children, Is.Not.Null);
        Assert.That(t.Children.Count(), Is.EqualTo(0));
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_EmptyHeap()
    {
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_FullHeap()
    {
        AddRandomIntValues(_intHeap);
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    [Test]
    public void UpdatePriorityOf_ChangeManyRandIntPriorities()
    {
        AddRandomIntValues(_intHeap);
        UpdateRandIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_ChangeOneRandIntPriority()
    {
        AddRandomIntValues(_intHeap, 0, 1);
        UpdateRandIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseAndIncreaseManyIntPriorities()
    {
        AddIntValues(_intHeap);
        DecreaseIntPriorities(_intHeap);
        IncreaseIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseAndIncreaseManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        DecreaseStringPriorities(_stringHeap);
        IncreaseStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseAndIncreaseOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        DecreaseIntPriorities(_intHeap, 0, 1);
        IncreaseIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseAndIncreaseOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        DecreaseStringPriorities(_stringHeap, 0, 1);
        IncreaseStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseManyIntPriorities()
    {
        AddIntValues(_intHeap);
        DecreaseIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        DecreaseStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        DecreaseIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecreaseOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        DecreaseStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecrIncrManyIntPriorities()
    {
        AddIntValues(_intHeap);
        DecrIncrIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecrIncrManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        DecrIncrStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecrIncrOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        DecrIncrIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_DecrIncrOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        DecrIncrStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncrDecrManyIntPriorities()
    {
        AddIntValues(_intHeap);
        IncrDecrIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncrDecrManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        IncrDecrStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncrDecrOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        IncrDecrIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncrDecrOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        IncrDecrStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseAndDecreaseManyIntPriorities()
    {
        AddIntValues(_intHeap);
        IncreaseIntPriorities(_intHeap);
        DecreaseIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseAndDecreaseManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        IncreaseStringPriorities(_stringHeap);
        DecreaseStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseAndDecreaseOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        IncreaseIntPriorities(_intHeap, 0, 1);
        DecreaseIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseAndDecreaseOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        IncreaseStringPriorities(_stringHeap, 0, 1);
        DecreaseStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseManyIntPriorities()
    {
        AddIntValues(_intHeap);
        IncreaseIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        IncreaseStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseOneIntPriority()
    {
        AddIntValues(_intHeap, 0, 1);
        IncreaseIntPriorities(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_IncreaseOneStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        IncreaseStringPriorities(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_ManyIntPriorities()
    {
        AddIntValues(_intHeap);
        IncreaseIntPriorities(_intHeap);
        IncrDecrIntPriorities(_intHeap);
        DecrIncrIntPriorities(_intHeap);
        DecreaseIntPriorities(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdatePriorityOf_ManyStringPriorities()
    {
        AddStringValues(_stringHeap);
        IncreaseStringPriorities(_stringHeap);
        IncrDecrStringPriorities(_stringHeap);
        DecrIncrStringPriorities(_stringHeap);
        DecreaseStringPriorities(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdatePriorityOf_NullStringPriority()
    {
        AddStringValues(_stringHeap, 0, 1);
        Should.Throw<ArgumentNullException>(() => _stringHeap.UpdatePriorityOf(s_stringHandles[0], null));
    }

    [Test]
    public void UpdatePriorityOf_NullStringValue()
    {
        Should.Throw<ArgumentException>(() => _stringHeap.UpdatePriorityOf(null, StringValues[0]));
    }

    [Test]
    public void UpdateValue_ManyRandIntValues()
    {
        AddIntValues(_intHeap);
        UpdateRandIntValues(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdateValue_ManyStringValues()
    {
        AddStringValues(_stringHeap);
        UpdateStringValues(_stringHeap);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdateValue_NullStringValue()
    {
        Should.Throw<ArgumentException>(() => _stringHeap.UpdateValue(null, StringValues[0]));
    }

    [Test]
    public void UpdateValue_OneRandIntValue()
    {
        AddIntValues(_intHeap, 0, 1);
        UpdateRandIntValues(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void UpdateValue_OneStringValue()
    {
        AddStringValues(_stringHeap, 0, 1);
        UpdateStringValues(_stringHeap, 0, 1);
        AssertSameContents(s_refStringHeap, _stringHeap);
    }

    [Test]
    public void UpdateValue_SameValues()
    {
        AddIntValues(_intHeap);
        foreach (var p in s_intHandles)
            _intHeap.UpdateValue(p.Value, p.Value.Value);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    protected abstract IRawHeap<T, T> GetHeap<T>() where T : IComparable<T>;

    protected abstract IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp);

    private static void AddIntValues(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            s_intHandles.Add(i, heap.Add(IntValues[i], IntValues[i]));
            s_refIntHandles.Add(i, s_refIntHeap.Add(IntValues[i], IntValues[i]));
        }
    }

    private static void AddRandomIntValues(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            s_randIntHandles.Add(i, heap.Add(RandomIntValues[i], RandomIntValues[i]));
            s_refRandIntHandles.Add(i, s_refIntHeap.Add(RandomIntValues[i], RandomIntValues[i]));
        }
    }

    private static void AddSameIntValues(IRawHeap<int, int> heap, int count = IntValueCount)
    {
        for (var i = 0; i < count; ++i)
        {
            heap.Add(0, 0);
            s_refIntHeap.Add(0, 0);
        }
    }

    private static void AddStringValues(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            s_stringHandles.Add(i, heap.Add(StringValues[i], StringValues[i]));
            s_refStringHandles.Add(i, s_refStringHeap.Add(StringValues[i], StringValues[i]));
        }
    }

    private static void AssertSameContents<T>(IRawHeap<T, T> refHeap, IRawHeap<T, T> heap)
        where T : IComparable<T>
    {
        Assert.That(heap, Has.Count.EqualTo(refHeap.Count));

        while (refHeap.Count != 0)
        {
            AssertSameHandle(refHeap.Min, heap.Min);
            if (refHeap.Count % 2 == 0)
            {
                AssertSameHandle(refHeap.RemoveMin(), heap.RemoveMin());
            }
            else
            {
                Assert.That(refHeap.Remove(refHeap.Min), Is.True);
                Assert.That(heap.Remove(heap.Min), Is.True);
            }
            Assert.That(heap, Has.Count.EqualTo(refHeap.Count));
        }

        Assert.That(heap, Is.Empty);
    }

    private static void AssertSameHandle<TVal, TPr>(IHeapHandle<TVal, TPr> p1, IHeapHandle<TVal, TPr> p2)
    {
        Assert.That(p2.Value, Is.EqualTo(p1.Value));
        Assert.That(p2.Priority, Is.EqualTo(p1.Priority));
    }

    private static void DecreaseIntPriorities(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            var oldPr = heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] - IntValueCount);
            var refOldPr = s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] - IntValueCount);
            Assert.That(oldPr, Is.EqualTo(refOldPr));
        }
    }

    private static void DecreaseStringPriorities(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.UpdatePriorityOf(s_stringHandles[i], '\0' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], '\0' + StringValues[i]);
        }
    }

    private static void DecrIncrIntPriorities(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] - IntValueCount);
            s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] - IntValueCount);
            heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] + IntValueCount);
            s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] + IntValueCount);
        }
    }

    private static void DecrIncrStringPriorities(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.UpdatePriorityOf(s_stringHandles[i], '\0' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], '\0' + StringValues[i]);
            heap.UpdatePriorityOf(s_stringHandles[i], 'z' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], 'z' + StringValues[i]);
        }
    }

    private static void HeapSort_Test<T>(IRawHeap<T, T> refHeap, IRawHeap<T, T> heap, List<T> values)
    {
        var refSortedValues = HeapSort.Sort(refHeap, values);
        var sortedValues = HeapSort.Sort(heap, values);
        Assert.That(sortedValues, Has.Length.EqualTo(refSortedValues.Length));
        for (var i = 0; i < refSortedValues.Length; ++i)
            Assert.That(sortedValues[i], Is.EqualTo(refSortedValues[i]));
    }

    private static void IncrDecrIntPriorities(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = to - 1; i >= from; --i)
        {
            heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] + IntValueCount);
            s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] + IntValueCount);
            heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] - IntValueCount);
            s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] - IntValueCount);
        }
    }

    private static void IncrDecrStringPriorities(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = to - 1; i >= from; --i)
        {
            heap.UpdatePriorityOf(s_stringHandles[i], 'z' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], 'z' + StringValues[i]);
            heap.UpdatePriorityOf(s_stringHandles[i], '\0' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], '\0' + StringValues[i]);
        }
    }

    private static void IncreaseIntPriorities(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = to - 1; i >= from; --i)
        {
            heap.UpdatePriorityOf(s_intHandles[i], IntValues[i] + IntValueCount);
            s_refIntHeap.UpdatePriorityOf(s_refIntHandles[i], IntValues[i] + IntValueCount);
        }
    }

    private static void IncreaseStringPriorities(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = to - 1; i >= from; --i)
        {
            heap.UpdatePriorityOf(s_stringHandles[i], 'z' + StringValues[i]);
            s_refStringHeap.UpdatePriorityOf(s_refStringHandles[i], 'z' + StringValues[i]);
        }
    }

    private static void UpdateRandIntPriorities(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            var r = NextRandInt();
            heap.UpdatePriorityOf(s_randIntHandles[i], r);
            s_refIntHeap.UpdatePriorityOf(s_refRandIntHandles[i], r);
        }

        // Repeat the loop to create more disorder...
        for (var i = from; i < to; ++i)
        {
            var r = NextRandInt();
            heap.UpdatePriorityOf(s_randIntHandles[i], r);
            s_refIntHeap.UpdatePriorityOf(s_refRandIntHandles[i], r);
        }
    }

    private static void UpdateRandIntValues(IRawHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.UpdateValue(s_intHandles[i], RandomIntValues[i]);
            s_refIntHeap.UpdateValue(s_refIntHandles[i], RandomIntValues[i]);
        }
    }

    private static void UpdateStringValues(IRawHeap<string, string> heap, int from = 0, int to = StringValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            var newValue = "GINA & PINO " + i;
            heap.UpdateValue(s_stringHandles[i], newValue);
            s_refStringHeap.UpdateValue(s_refStringHandles[i], newValue);
        }
    }

    private void Dijkstra(RandomGraph graph)
    {
        var refDist = graph.Dijkstra(s_refIntHeap, 0);
        var dist = graph.Dijkstra(_intHeap, 0);
        Assert.That(dist, Has.Length.EqualTo(refDist.Length));
        for (var i = 0; i < refDist.Length; ++i)
            Assert.That(dist[i], Is.EqualTo(refDist[i]));
    }
}

public sealed class RawPairingHeapTests : RawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawPairingHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawPairingHeap<T, T>(cmp);
    }
}

public sealed class StableRawArrayHeapTests : StableRawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawArrayHeap<T, T>(7);
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawArrayHeap<T, T>(7, cmp);
    }
}

public sealed class StableRawBinaryHeapTests : StableRawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawBinaryHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawBinaryHeap<T, T>(cmp);
    }
}

public sealed class StableRawBinomialHeapTests : StableRawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawBinomialHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawBinomialHeap<T, T>(cmp);
    }
}

public sealed class StableRawFibonacciHeapTests : StableRawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawFibonacciHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawFibonacciHeap<T, T>(cmp);
    }
}

public abstract class StableRawHeapTests : RawHeapTests
{
    private IStableRawHeap<int, int> StableIntHeap
    {
        get { return (IStableRawHeap<int, int>)_intHeap; }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority(int valueCount)
    {
        for (var i = 0; i < valueCount; ++i)
        {
            _intHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority_CustomVersion(int valueCount)
    {
        var version = 0L;
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.Add(i, 0, version++);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority_StableHeap(int valueCount)
    {
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Indexer_SamePriority_ReverseOrder(int valueCount)
    {
        var handles = new IHeapHandle<int, int>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = _intHeap.Add(i, 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            _intHeap[handles[i]] = 0;
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Indexer_SamePriority_ReverseOrder_StableHeap(int valueCount)
    {
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            StableIntHeap[handles[i]] = 0;
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Indexer_SamePriority_SameOrder(int valueCount)
    {
        var handles = new IHeapHandle<int, int>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = _intHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            _intHeap[handles[i]] = 0;
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Indexer_SamePriority_SameOrder_StableHeap(int valueCount)
    {
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap[handles[i]] = 0;
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_ReverseOrder(int valueCount)
    {
        var handles = new IHeapHandle<int, int>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = _intHeap.Add(i, 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            _intHeap.UpdatePriorityOf(handles[i], 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_ReverseOrder_CustomVersion(int valueCount)
    {
        var version = 0L;
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0, version++);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            StableIntHeap.UpdatePriorityOf(handles[i], 0, version++);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_ReverseOrder_StableHeap(int valueCount)
    {
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            StableIntHeap.UpdatePriorityOf(handles[i], 0);
        }
        for (var i = valueCount - 1; i >= 0; --i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_SameOrder(int valueCount)
    {
        var handles = new IHeapHandle<int, int>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = _intHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            _intHeap.UpdatePriorityOf(handles[i], 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_SameOrder_CustomVersion(int valueCount)
    {
        var version = 0L;
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0, version++);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.UpdatePriorityOf(handles[i], 0, version++);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void UpdatedPriorityOf_SamePriority_SameOrder_StableHeap(int valueCount)
    {
        var handles = new IHeapHandle<int, IVersionedPriority<int>>[valueCount];
        for (var i = 0; i < valueCount; ++i)
        {
            handles[i] = StableIntHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.UpdatePriorityOf(handles[i], 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }
}

public sealed class StableRawPairingHeapTests : StableRawHeapTests
{
    protected override IRawHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawPairingHeap<T, T>();
    }

    protected override IRawHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawPairingHeap<T, T>(cmp);
    }
}
