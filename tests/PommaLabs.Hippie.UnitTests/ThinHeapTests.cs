﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests;

public sealed class ArrayAsStableThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewArrayHeap<T, T>(7);
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewArrayHeap<T, T>(7, cmp);
    }
}

public sealed class ArrayAsThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewArrayHeap<T, T>(7);
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewArrayHeap<T, T>(7, cmp);
    }
}

public sealed class BinaryAsStableThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewBinaryHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewBinaryHeap<T, T>(cmp);
    }
}

public sealed class BinaryAsThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewBinaryHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewBinaryHeap<T, T>(cmp);
    }
}

public sealed class RawArrayAsStableThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawArrayHeap<T, T>(7);
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawArrayHeap<T, T>(7, cmp);
    }
}

public sealed class RawArrayAsThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawArrayHeap<T, T>(7);
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawArrayHeap<T, T>(7, cmp);
    }
}

public sealed class RawBinaryAsStableThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewRawBinaryHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewRawBinaryHeap<T, T>(cmp);
    }
}

public sealed class RawBinaryAsThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewRawBinaryHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewRawBinaryHeap<T, T>(cmp);
    }
}

public sealed class RealStableThinHeapTests : StableThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return StableHeapFactory.NewHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return StableHeapFactory.NewHeap<T, T>(cmp);
    }
}

public sealed class RealThinHeapTests : ThinHeapTests
{
    protected override IThinHeap<T, T> GetHeap<T>()
    {
        return HeapFactory.NewHeap<T, T>();
    }

    protected override IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp)
    {
        return HeapFactory.NewHeap<T, T>(cmp);
    }
}

public abstract class StableThinHeapTests : ThinHeapTests
{
    private IStableThinHeap<int, int> StableIntHeap
    {
        get { return (IStableThinHeap<int, int>)_intHeap; }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority(int valueCount)
    {
        for (var i = 0; i < valueCount; ++i)
        {
            _intHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(_intHeap.Min.Value, Is.EqualTo(i));
            Assert.That(_intHeap.Min.Priority, Is.EqualTo(0));
            var min = _intHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority_CustomVersion(int valueCount)
    {
        var version = 0L;
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.Add(i, 0, version++);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }

    [TestCase(1)]
    [TestCase(IntValueCount / 2)]
    [TestCase(IntValueCount)]
    public void Add_SamePriority_StableHeap(int valueCount)
    {
        for (var i = 0; i < valueCount; ++i)
        {
            StableIntHeap.Add(i, 0);
        }
        for (var i = 0; i < valueCount; ++i)
        {
            Assert.That(StableIntHeap.Min.Value, Is.EqualTo(i));
            Assert.That(StableIntHeap.Min.Priority.Value, Is.EqualTo(0));
            var min = StableIntHeap.RemoveMin();
            Assert.That(min.Value, Is.EqualTo(i));
            Assert.That(min.Priority.Value, Is.EqualTo(0));
        }
    }
}

public abstract class ThinHeapTests : ValPrHeapTests
{
    protected IThinHeap<int, int> _intHeap;

    private static readonly Dictionary<int, IHeapHandle<int, int>> s_intHandles;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_randIntHandles;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_refIntHandles;
    private static readonly ArrayHeap<int, int> s_refIntHeap;
    private static readonly Dictionary<int, IHeapHandle<int, int>> s_refRandIntHandles;
    private static readonly Dictionary<int, IHeapHandle<string, string>> s_refStringHandles;
    private static readonly ArrayHeap<string, string> s_refStringHeap;
    private static readonly Dictionary<int, IHeapHandle<string, string>> s_stringHandles;

    private IThinHeap<string, string> _stringHeap;

    static ThinHeapTests()
    {
        s_intHandles = [];
        s_refIntHandles = [];
        s_stringHandles = [];
        s_refStringHandles = [];
        s_randIntHandles = [];
        s_refRandIntHandles = [];
        s_refIntHeap = HeapFactory.NewRawBinaryHeap<int, int>();
        s_refStringHeap = HeapFactory.NewRawBinaryHeap<string, string>();
    }

    protected ThinHeapTests()
    {
        RandSet.Clear();
    }

    [Test]
    public void Add_ManyIntItems()
    {
        AddIntValues(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_ManyRandIntItems()
    {
        AddRandomIntValues(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_NullArguments()
    {
        Should.Throw<ArgumentNullException>(() => _stringHeap.Add(null, null));
    }

    [Test]
    public void Add_NullStringPriority()
    {
        Should.Throw<ArgumentNullException>(() => _stringHeap.Add(StringValues[0], null));
    }

    [Test]
    public void Add_OneIntItem()
    {
        AddIntValues(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_OneRandIntItem()
    {
        AddRandomIntValues(_intHeap, 0, 1);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Add_ReversedIntComparer()
    {
        _intHeap = GetHeap(new ReversedIntComparer());
        _intHeap.Add(1, 10);
        _intHeap.Add(2, 20);
        _intHeap.Add(3, 30);
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(30));
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(20));
        Assert.That(_intHeap.RemoveMin().Priority, Is.EqualTo(10));
    }

    [Test]
    public void Clear_EmptyHeap()
    {
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
    }

    [Test]
    public void Clear_FullHeap()
    {
        AddIntValues(_intHeap);
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
    }

    [Test]
    public void Clear_TwoTimes()
    {
        AddIntValues(_intHeap);
        _intHeap.Clear();
        _intHeap.Clear();
        Assert.That(_intHeap, Is.Empty);
    }

    [Test]
    public void HeapSort_IntValues()
    {
        HeapSort_Test(s_refIntHeap, _intHeap, IntValues);
    }

    [Test]
    public void HeapSort_RandomIntValues()
    {
        HeapSort_Test(s_refIntHeap, _intHeap, RandomIntValues);
    }

    [Test]
    public void Merge_CovariantHeaps()
    {
        var aHeap = HeapFactory.NewHeap<A, int>();
        var bHeap = HeapFactory.NewHeap<B, int>();
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, i);
            var bItem = new B(rand);
            bHeap.Add(bItem, i);
        }
        aHeap.Merge(bHeap);
        Assert.That(aHeap, Has.Count.EqualTo(200));
        Assert.That(bHeap, Is.Empty);
    }

    [Test]
    public void Merge_CovariantHeaps_TwoTimes()
    {
        var aHeap = HeapFactory.NewHeap<A, int>();
        var bHeap = HeapFactory.NewHeap<B, int>();
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, i);
            var bItem = new B(rand);
            bHeap.Add(bItem, i);
        }
        aHeap.Merge(bHeap);
        for (var i = 0; i < 100; ++i)
        {
            var rand = NextRandInt();
            var aItem = new A(rand);
            aHeap.Add(aItem, i);
            var bItem = new B(rand);
            bHeap.Add(bItem, i);
        }
        aHeap.Merge(bHeap);
        Assert.That(aHeap, Has.Count.EqualTo(400));
        Assert.That(bHeap, Is.Empty);
    }

    [Test]
    public void Merge_EmptyHeap_WithEmptyHeap_OfDifferentType()
    {
        var heap = HeapFactory.NewHeap<int, int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithEmptyHeap_OfSameType()
    {
        var heap = GetHeap<int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithFullHeap_OfDifferentType()
    {
        var heap = HeapFactory.NewHeap<int, int>();
        AddIntValues(heap);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithFullHeap_OfSameType()
    {
        var heap = GetHeap<int>();
        AddIntValues(heap);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_EmptyHeap_WithNullHeap()
    {
        Should.Throw<ArgumentNullException>(() => _intHeap.Merge<int, int>(null));
    }

    [Test]
    public void Merge_FullHeap_WithEmptyHeap_OfDifferentType()
    {
        AddIntValues(_intHeap);
        var heap = HeapFactory.NewHeap<int, int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithEmptyHeap_OfSameType()
    {
        AddIntValues(_intHeap);
        var heap = GetHeap<int>();
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithFullHeap_OfDifferentType()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = HeapFactory.NewHeap<int, int>();
        AddIntValues(heap, IntValueCount / 2);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithFullHeap_OfSameType()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = GetHeap<int>();
        AddIntValues(heap, IntValueCount / 2);
        _intHeap.Merge(heap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Merge_FullHeap_WithNullHeap()
    {
        AddIntValues(_intHeap);
        Should.Throw<ArgumentNullException>(() => _intHeap.Merge<int, int>(null));
    }

    [Test]
    public void Merge_WithFullHeap_WithDifferentComparer()
    {
        AddIntValues(_intHeap, 0, IntValueCount / 2);
        var heap = HeapFactory.NewHeap<int, int>(new ReversedIntComparer());
        AddIntValues(heap, IntValueCount / 2);
        Should.Throw<ArgumentException>(() => _intHeap.Merge(heap));
    }

    [Test]
    public void Merge_WithSameHeap()
    {
        AddIntValues(_intHeap);
        _intHeap.Merge(_intHeap);
        AssertSameContents(s_refIntHeap, _intHeap);
    }

    [Test]
    public void Min_EmptyHeap()
    {
        Should.Throw<InvalidOperationException>(() => _intHeap.Min);
    }

    [Test]
    public void RemoveMin_EmptyHeap()
    {
        Should.Throw<InvalidOperationException>(() => _intHeap.RemoveMin());
    }

    [SetUp]
    public void SetUp()
    {
        _intHeap = GetHeap<int>();
        _stringHeap = GetHeap<string>();
    }

    [TearDown]
    public void TearDown()
    {
        s_intHandles.Clear();
        s_refIntHandles.Clear();
        s_stringHandles.Clear();
        s_refStringHandles.Clear();
        s_randIntHandles.Clear();
        s_refRandIntHandles.Clear();
        s_refIntHeap.Clear();
        s_refStringHeap.Clear();
        _intHeap = null;
        _stringHeap = null;
    }

    [Test]
    [Repeat(3)]
    public void ToReadOnlyForest_Generic()
    {
        AddRandomIntValues(_intHeap);
        var values = new HashSet<int>(_intHeap.Select(p => p.Value));
        var priorities = new HashSet<int>(_intHeap.Select(p => p.Priority));
        var forest = _intHeap.ToReadOnlyForest().ToList();
        // Breadth-first, no action
        var trees = forest.SelectMany(t => t.BreadthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(IntValueCount));
        foreach (var t in trees)
        {
            Assert.That(values.Contains(t.Value));
            Assert.That(priorities.Contains(t.Priority));
        }
        // Depth-first, no action
        trees = forest.SelectMany(t => t.DepthFirstVisit()).ToList();
        Assert.That(trees, Has.Count.EqualTo(IntValueCount));
        foreach (var t in trees)
        {
            Assert.That(values.Contains(t.Value));
            Assert.That(priorities.Contains(t.Priority));
        }
        // Breadth-first, with selector
        var res = forest.SelectMany(t => t.BreadthFirstVisit((n, a) => n.Priority, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(IntValueCount));
        foreach (var r in res)
        {
            Assert.That(priorities.Contains(r));
        }
        // Depth-first, with selector
        res = forest.SelectMany(t => t.DepthFirstVisit((n, a) => n.Priority, 0)).ToList();
        Assert.That(res, Has.Count.EqualTo(IntValueCount));
        foreach (var r in res)
        {
            Assert.That(priorities.Contains(r));
        }
        // Breadth-first, with action
        var set = new HashSet<int>();
        forest.ForEach(t => t.BreadthFirstVisit(n => set.Add(n.Priority)));
        Assert.That(set, Has.Count.EqualTo(priorities.Count));
        foreach (var s in set)
        {
            Assert.That(priorities.Contains(s));
        }
        // Depth-first, with action
        set = [];
        forest.ForEach(t => t.DepthFirstVisit(n => set.Add(n.Priority)));
        Assert.That(set, Has.Count.EqualTo(priorities.Count));
        foreach (var s in set)
        {
            Assert.That(priorities.Contains(s));
        }
    }

    [Test]
    public void ToReadOnlyForest_OneElement()
    {
        _intHeap.Add(IntValues[0], 0);
        var f = _intHeap.ToReadOnlyForest().ToList();
        Assert.That(f, Has.Count.EqualTo(1));
        var t = f[0];
        Assert.That(t, Is.Not.Null);
        Assert.That(t.Parent, Is.Null);
        Assert.That(t.Value, Is.EqualTo(IntValues[0]));
        Assert.That(t.Priority, Is.EqualTo(0));
        Assert.That(t.Children, Is.Not.Null);
        Assert.That(t.Children.Count(), Is.EqualTo(0));
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_EmptyHeap()
    {
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    [Test]
    public void ToReadOnlyForest_ResultNotNull_FullHeap()
    {
        AddRandomIntValues(_intHeap);
        Assert.That(_intHeap.ToReadOnlyForest(), Is.Not.Null);
    }

    protected abstract IThinHeap<T, T> GetHeap<T>() where T : IComparable<T>;

    protected abstract IThinHeap<T, T> GetHeap<T>(IComparer<T> cmp) where T : IComparable<T>;

    private static void AddIntValues(IThinHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.Add(IntValues[i], IntValues[i]);
            s_refIntHandles.Add(i, s_refIntHeap.Add(IntValues[i], IntValues[i]));
        }
    }

    private static void AddRandomIntValues(IThinHeap<int, int> heap, int from = 0, int to = IntValueCount)
    {
        for (var i = from; i < to; ++i)
        {
            heap.Add(RandomIntValues[i], RandomIntValues[i]);
            s_refRandIntHandles.Add(i, s_refIntHeap.Add(RandomIntValues[i], RandomIntValues[i]));
        }
    }

    private static void AssertSameContents<T>(ArrayHeap<T, T> refHeap, IThinHeap<T, T> heap) where T : struct, IComparable<T>
    {
        Assert.That(heap, Has.Count.EqualTo(refHeap.Count));

        while (refHeap.Count != 0)
        {
            AssertSameHandle(refHeap.Min, heap.Min);
            AssertSameHandle(refHeap.RemoveMin(), heap.RemoveMin());
            Assert.That(heap, Has.Count.EqualTo(refHeap.Count));
        }

        Assert.That(heap, Is.Empty);
    }

    private static void AssertSameHandle<TVal, TPr>(IHeapHandle<TVal, TPr> p1, IHeapHandle<TVal, TPr> p2) where TPr : struct
    {
        Assert.That(p2.Value, Is.EqualTo(p1.Value));
        Assert.That(p2.Priority, Is.EqualTo(p1.Priority));
    }

    private static void HeapSort_Test<T>(IRawHeap<T, T> refHeap, IThinHeap<T, T> heap, List<T> values) where T : struct
    {
        var refSortedValues = HeapSort.Sort(refHeap, values);
        var sortedValues = HeapSort.Sort(heap, values);
        Assert.That(sortedValues, Has.Length.EqualTo(refSortedValues.Length));
        for (var i = 0; i < refSortedValues.Length; ++i)
            Assert.That(sortedValues[i], Is.EqualTo(refSortedValues[i]));
    }
}
