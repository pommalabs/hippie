﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;

namespace PommaLabs.Hippie.UnitTests;

[TestFixture]
public abstract class HeapTestsBase
{
    protected const int StringItemLength = 21;
    protected const int StringValueCount = 21; // Our birthdays... :)
    protected static readonly Random Rand = new();

    protected class A(int x) : IComparable<A>
    {
        private readonly int _x = x;

        public int CompareTo(A other)
        {
            return _x.CompareTo(other._x);
        }
    }

    protected sealed class B(int x) : A(x)
    {
    }

    protected sealed class FakeIntEqualityComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return true;
        }

        public int GetHashCode(int obj)
        {
            return 0;
        }
    }

    protected sealed class ReversedIntComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            return -x.CompareTo(y);
        }
    }
}

public abstract class ValPrHeapTests : HeapTestsBase
{
    protected const int IntValueCount = 6120;
    protected static readonly RandomGraph EmptyGraph = new(DijkstraValueCount, 0.0);
    protected static readonly List<int> IntValues = [];
    protected static readonly RandomGraph MediumGraph = new(DijkstraValueCount, 0.3);
    protected static readonly List<int> RandomIntValues = [];
    protected static readonly HashSet<int> RandSet = [];
    protected static readonly RandomGraph SparseGraph = new(DijkstraValueCount, 0.15);
    protected static readonly List<string> StringValues = [];
    private const int DijkstraValueCount = 3060;

    static ValPrHeapTests()
    {
        for (var i = IntValueCount - 1; i >= 0; --i)
            IntValues.Add(i);

        for (var i = 0; i < IntValueCount; ++i)
            RandomIntValues.Add(NextRandInt());

        for (var i = StringValueCount - 1; i >= 0; --i)
            StringValues.Add(new string((char)('a' + i), StringItemLength));
    }

    protected static int NextRandInt()
    {
        int r;
        while (RandSet.Contains(r = Rand.Next() + IntValueCount)) { }
        RandSet.Add(r);
        return r;
    }
}
